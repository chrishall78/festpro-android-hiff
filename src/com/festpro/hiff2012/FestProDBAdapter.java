package com.festpro.hiff2012;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class FestProDBAdapter {

    final Context context;    
    FestProDBHelper DBHelper;
    SQLiteDatabase db;

    //private static final String DEBUG_TAG = "FestProDatabase";
    private static final String DATABASE_NAME = "FestPro";
    // do not update this version unless the database fields change or the festival is changing
    static final int DATABASE_VERSION = 34;
 
    /* --- Films --- */
    public static final String TABLE_FILMS = "Films";
    // Text Values
    public static final String FILMS_ID = "_id";
    public static final String FILMS_COUNTRIES = "countries";
    public static final String FILMS_DIRECTOR_NAME = "directorName";
    public static final String FILMS_EVENT_TYPE = "eventType";
    public static final String FILMS_GENRES = "genres";
    public static final String FILMS_IMAGE_PATH = "imagePath";
    public static final String FILMS_IMAGE_URL = "imageUrl";
    public static final String FILMS_LANGUAGES = "languages";
    public static final String FILMS_PREMIERE_TYPE = "premiereType";
    public static final String FILMS_SECTION = "section";
    public static final String FILMS_SLUG = "slug";
    public static final String FILMS_SYNOPSIS_LONG = "synopsisLong";
    public static final String FILMS_SYNOPSIS_SHORT = "synopsisShort";
    public static final String FILMS_SYNOPSIS_ORIGINAL = "synopsisOriginal";
    public static final String FILMS_TITLE_ALPHA = "titleAlpha";
    public static final String FILMS_TITLE_ENGLISH = "titleEnglish";
    public static final String FILMS_TITLE_ROMANIZED = "titleRomanized";
    // Integer Values
    public static final String FILMS_FILM_ID = "film_id";
    public static final String FILMS_RUNTIME = "runtime";
    public static final String FILMS_YEAR = "year";
    
    static final String CREATE_TABLE_FILMS = "create table " + TABLE_FILMS
    + " (" + FILMS_ID + " integer primary key autoincrement, " + FILMS_COUNTRIES
    + " text not null, " + FILMS_DIRECTOR_NAME + " text not null, " + FILMS_EVENT_TYPE
    + " text not null, " + FILMS_GENRES + " text not null, " + FILMS_IMAGE_PATH
    + " text not null, " + FILMS_IMAGE_URL + " text not null, " + FILMS_LANGUAGES
    + " text not null, " + FILMS_PREMIERE_TYPE + " text not null, " + FILMS_SECTION
    + " text not null, " + FILMS_SLUG + " text not null, " + FILMS_SYNOPSIS_LONG
    + " text not null, " + FILMS_SYNOPSIS_SHORT + " text not null, " + FILMS_SYNOPSIS_ORIGINAL
    + " text not null, " + FILMS_TITLE_ALPHA + " text not null, " + FILMS_TITLE_ENGLISH
    + " text not null, " + FILMS_TITLE_ROMANIZED + " text not null, " + FILMS_FILM_ID
    + " int not null, " + FILMS_RUNTIME + " int not null, " + FILMS_YEAR
    + " int not null" + "); ";


    /* --- Screenings --- */
    public static final String TABLE_SCREENINGS = "Screenings";
    // Text Values
    public static final String SCREENINGS_ID = "_id";
    public static final String SCREENINGS_DATESTRING = "dateString";    
    public static final String SCREENINGS_DATETIME = "dateTime";
    public static final String SCREENINGS_FILM_IDS = "film_ids";    
    public static final String SCREENINGS_FILMS = "films";
    public static final String SCREENINGS_LOCATION = "location";
    public static final String SCREENINGS_PROGRAM_NAME = "programName";
    public static final String SCREENINGS_TICKET_LINK = "ticketLink";
    public static final String SCREENINGS_SLUG = "slug";
    // Integer Values
    public static final String SCREENINGS_FREE = "free";    
    public static final String SCREENINGS_PRIVATE = "private";    
    public static final String SCREENINGS_RUSH = "rush";    
    public static final String SCREENINGS_FILM_ID = "film_id";    
    public static final String SCREENINGS_SCREENING_ID = "screening_id";
    public static final String SCREENINGS_TOTAL_RUNTIME = "totalRuntime";    

    public static final String CREATE_TABLE_SCREENINGS = "create table " + TABLE_SCREENINGS
    + " (" + SCREENINGS_ID + " integer primary key autoincrement, " + SCREENINGS_DATESTRING
    + " text not null, " + SCREENINGS_DATETIME + " text not null, " + SCREENINGS_FILM_IDS
    + " text not null, " + SCREENINGS_FILMS + " text not null, " + SCREENINGS_LOCATION
    + " text not null, " + SCREENINGS_PROGRAM_NAME + " text not null, " + SCREENINGS_TICKET_LINK
    + " text not null, " + SCREENINGS_SLUG + " text not null, " + SCREENINGS_FREE
    + " int not null, " + SCREENINGS_PRIVATE + " int not null, " + SCREENINGS_RUSH
    + " int not null, " + SCREENINGS_FILM_ID + " int not null, " + SCREENINGS_SCREENING_ID
    + " int not null, " + SCREENINGS_TOTAL_RUNTIME + " int not null" + "); ";
 
    
    /* --- Personnel --- */
    public static final String TABLE_PERSONNEL = "Personnel";
    // Text Values
    public static final String PERSONNEL_ID = "_id";
    public static final String PERSONNEL_FIRSTNAME = "firstName";
    public static final String PERSONNEL_LASTNAME = "lastName";
    public static final String PERSONNEL_ROLE = "role";
    // Integer Values
    public static final String PERSONNEL_FILM_ID = "film_id";    

    public static final String CREATE_TABLE_PERSONNEL = "create table " + TABLE_PERSONNEL
    + " (" + PERSONNEL_ID + " integer primary key autoincrement, " + PERSONNEL_FIRSTNAME
    + " text not null, " + PERSONNEL_LASTNAME + " text not null, " + PERSONNEL_ROLE
    + " text not null, " + PERSONNEL_FILM_ID + " int not null" + "); ";

    
    /* --- Photos/Videos --- */
    public static final String TABLE_PHOTOVIDEOS = "Photovideos";
    // Text Values
    public static final String PHOTOVIDEOS_ID = "_id";
    public static final String PHOTOVIDEOS_PHOTO_LARGE_URL = "photoLargeUrl";
    public static final String PHOTOVIDEOS_PHOTO_SMALL_URL = "photoSmallUrl";
    public static final String PHOTOVIDEOS_TYPE = "type";
    public static final String PHOTOVIDEOS_VIDEO_SERVICE = "videoService";
    public static final String PHOTOVIDEOS_VIDEO_URL = "videoUrl";
    // Integer Values
    public static final String PHOTOVIDEOS_FILM_ID = "film_id";
    public static final String PHOTOVIDEOS_ORDER = "ordernum";

    public static final String CREATE_TABLE_PHOTOVIDEOS = "create table " + TABLE_PHOTOVIDEOS
    + " (" + PHOTOVIDEOS_ID + " integer primary key autoincrement, " + PHOTOVIDEOS_PHOTO_LARGE_URL
    + " text not null, " + PHOTOVIDEOS_PHOTO_SMALL_URL + " text not null, " + PHOTOVIDEOS_TYPE
    + " text not null, " + PHOTOVIDEOS_VIDEO_SERVICE + " text not null, " + PHOTOVIDEOS_VIDEO_URL
    + " text not null, " + PHOTOVIDEOS_FILM_ID + " int not null, " + PHOTOVIDEOS_ORDER
    + " int not null" + "); ";


    /* --- Sponsors --- */
    public static final String TABLE_SPONSORS = "Sponsors";
    // Text Values
    public static final String SPONSORS_ID = "_id";
    public static final String SPONSORS_DESC = "sponsorDesc";
    public static final String SPONSORS_LOGO_URL = "sponsorLogoUrl";
    public static final String SPONSORS_NAME = "sponsorName";
    public static final String SPONSORS_SITE_URL = "sponsorSiteUrl";
    // Integer Values
    public static final String SPONSORS_FILM_ID = "film_id";

    public static final String CREATE_TABLE_SPONSORS = "create table " + TABLE_SPONSORS
    + " (" + SPONSORS_ID + " integer primary key autoincrement, " + SPONSORS_DESC
    + " text not null, " + SPONSORS_LOGO_URL + " text not null, " + SPONSORS_NAME
    + " text not null, " + SPONSORS_SITE_URL + " text not null, " + SPONSORS_FILM_ID
    + " int not null" + "); ";

    
    public FestProDBAdapter(Context ctx) {
    	this.context = ctx;
    	DBHelper = new FestProDBHelper(context);
    }

    static class FestProDBHelper extends SQLiteOpenHelper {
        
        FestProDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }
     
        @Override
        public void onCreate(SQLiteDatabase db) {
        	try {
                db.execSQL(CREATE_TABLE_FILMS);
                db.execSQL(CREATE_TABLE_SCREENINGS);
                db.execSQL(CREATE_TABLE_PERSONNEL);
                db.execSQL(CREATE_TABLE_PHOTOVIDEOS);
                db.execSQL(CREATE_TABLE_SPONSORS);
        	} catch (SQLException e) {
        		e.printStackTrace();
        	}
        }
     
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Log.w(DEBUG_TAG, "Upgrading database. Existing contents will be lost. ["
            //        + oldVersion + "]->[" + newVersion + "]");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_FILMS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCREENINGS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSONNEL);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_PHOTOVIDEOS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_SPONSORS);
            onCreate(db);
        }
    }
    
    public FestProDBAdapter open() throws SQLException {
    	db = DBHelper.getWritableDatabase();
    	return this;
    }
    
    public void close() {
    	DBHelper.close();
    }
}