package com.festpro.hiff2012;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

public class FestProDBProvider extends ContentProvider {

	SQLiteDatabase sqlDB;
	private static final String AUTHORITY = "com.festpro.hiff2012.FestProDBProvider";

	public static final int FILMS = 100;
	public static final int FILM_ID = 110;
	public static final int SCREENINGS = 200;
	public static final int SCREENING_ID = 210;
	public static final int PERSONNEL = 300;
	public static final int PERSONNEL_ID = 310;
	public static final int PHOTOVIDEOS = 400;
	public static final int PHOTOVIDEO_ID = 410;
	public static final int SPONSORS = 500;
	public static final int SPONSOR_ID = 510;

	public static final Uri CONTENT_URI_FILMS = Uri.parse("content://"
			+ AUTHORITY + "/films");
	public static final Uri CONTENT_URI_SCREENINGS = Uri.parse("content://"
			+ AUTHORITY + "/screenings");
	public static final Uri CONTENT_URI_PERSONNEL = Uri.parse("content://"
			+ AUTHORITY + "/personnel");
	public static final Uri CONTENT_URI_PHOTOVIDEOS = Uri.parse("content://"
			+ AUTHORITY + "/photovideos");
	public static final Uri CONTENT_URI_SPONSORS = Uri.parse("content://"
			+ AUTHORITY + "/sponsors");

	private static final UriMatcher sURIMatcher;
	static {
		sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		sURIMatcher.addURI(AUTHORITY, "films", FILMS);
		sURIMatcher.addURI(AUTHORITY, "films/#", FILM_ID);
		sURIMatcher.addURI(AUTHORITY, "screenings", SCREENINGS);
		sURIMatcher.addURI(AUTHORITY, "screenings/#", SCREENING_ID);
		sURIMatcher.addURI(AUTHORITY, "personnel", PERSONNEL);
		sURIMatcher.addURI(AUTHORITY, "personnel/#", PERSONNEL_ID);
		sURIMatcher.addURI(AUTHORITY, "photovideos", PHOTOVIDEOS);
		sURIMatcher.addURI(AUTHORITY, "photovideos/#", PHOTOVIDEO_ID);
		sURIMatcher.addURI(AUTHORITY, "sponsors", SPONSORS);
		sURIMatcher.addURI(AUTHORITY, "sponsors/#", SPONSOR_ID);
	}


	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		int rowsAffected = 0;
		switch (sURIMatcher.match(uri)) {
		case FILMS:
			rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_FILMS,
					selection, selectionArgs);
			break;
		case FILM_ID:
			String id = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_FILMS,
						FestProDBAdapter.FILMS_ID + "=" + id, null);
			} else {
				rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_FILMS,
						selection + " and " + FestProDBAdapter.FILMS_ID + "="
								+ id, selectionArgs);
			}
			break;
		case SCREENINGS:
			rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_SCREENINGS,
					selection, selectionArgs);
			break;
		case SCREENING_ID:
			String id2 = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_SCREENINGS,
						FestProDBAdapter.SCREENINGS_ID + "=" + id2, null);
			} else {
				rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_SCREENINGS,
						selection + " and " + FestProDBAdapter.SCREENINGS_ID + "="
								+ id2, selectionArgs);
			}
			break;
		case PERSONNEL:
			rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_PERSONNEL,
					selection, selectionArgs);
			break;
		case PERSONNEL_ID:
			String id3 = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_PERSONNEL,
						FestProDBAdapter.PERSONNEL_ID + "=" + id3, null);
			} else {
				rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_PERSONNEL,
						selection + " and " + FestProDBAdapter.PERSONNEL_ID + "="
								+ id3, selectionArgs);
			}
			break;
		case PHOTOVIDEOS:
			rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_PHOTOVIDEOS,
					selection, selectionArgs);
			break;
		case PHOTOVIDEO_ID:
			String id4 = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_PHOTOVIDEOS,
						FestProDBAdapter.PHOTOVIDEOS_ID + "=" + id4, null);
			} else {
				rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_PHOTOVIDEOS,
						selection + " and " + FestProDBAdapter.PHOTOVIDEOS_ID + "="
								+ id4, selectionArgs);
			}
			break;
		case SPONSORS:
			rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_SPONSORS,
					selection, selectionArgs);
			break;
		case SPONSOR_ID:
			String id5 = uri.getLastPathSegment();
			if (TextUtils.isEmpty(selection)) {
				rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_SPONSORS,
						FestProDBAdapter.SPONSORS_ID + "=" + id5, null);
			} else {
				rowsAffected = sqlDB.delete(FestProDBAdapter.TABLE_SPONSORS,
						selection + " and " + FestProDBAdapter.SPONSORS_ID + "="
								+ id5, selectionArgs);
			}
			break;
		default:
			throw new IllegalArgumentException("Unknown or Invalid URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsAffected;
	}

	@Override
	public String getType(Uri uri) {
		switch (sURIMatcher.match(uri)) {
		// ---get all items---
		case FILMS:
			return ContentResolver.CURSOR_DIR_BASE_TYPE + "/com.festpro.hiff2012 ";
		case SCREENINGS:
			return ContentResolver.CURSOR_DIR_BASE_TYPE + "/com.festpro.hiff2012 ";
		case PERSONNEL:
			return ContentResolver.CURSOR_DIR_BASE_TYPE + "/com.festpro.hiff2012 ";
		case PHOTOVIDEOS:
			return ContentResolver.CURSOR_DIR_BASE_TYPE + "/com.festpro.hiff2012 ";
		case SPONSORS:
			return ContentResolver.CURSOR_DIR_BASE_TYPE + "/com.festpro.hiff2012 ";

		// ---get a particular item---
		case FILM_ID:
			return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/com.festpro.hiff2012 ";
		case SCREENING_ID:
			return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/com.festpro.hiff2012 ";
		case PERSONNEL_ID:
			return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/com.festpro.hiff2012 ";
		case PHOTOVIDEO_ID:
			return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/com.festpro.hiff2012 ";
		case SPONSOR_ID:
			return ContentResolver.CURSOR_ITEM_BASE_TYPE + "/com.festpro.hiff2012 ";

		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		Uri _uri = null;
		switch (sURIMatcher.match(uri)) {
		case FILMS:
			// ---add a new item---
			long rowID1 = sqlDB.insert(FestProDBAdapter.TABLE_FILMS, "", values);

			// ---if added successfully---
			if (rowID1 > 0) {
				_uri = ContentUris.withAppendedId(CONTENT_URI_FILMS, rowID1);
				getContext().getContentResolver().notifyChange(_uri, null);
			}
			break;
		case SCREENINGS:
			long rowID2 = sqlDB.insert(FestProDBAdapter.TABLE_SCREENINGS, "", values);
			if (rowID2 > 0) {
				_uri = ContentUris.withAppendedId(CONTENT_URI_SCREENINGS, rowID2);
				getContext().getContentResolver().notifyChange(_uri, null);
			}
			break;
		case PERSONNEL:
			long rowID3 = sqlDB.insert(FestProDBAdapter.TABLE_PERSONNEL, "", values);
			if (rowID3 > 0) {
				_uri = ContentUris.withAppendedId(CONTENT_URI_PERSONNEL, rowID3);
				getContext().getContentResolver().notifyChange(_uri, null);
			}
			break;
		case PHOTOVIDEOS:
			long rowID4 = sqlDB.insert(FestProDBAdapter.TABLE_PHOTOVIDEOS, "", values);
			if (rowID4 > 0) {
				_uri = ContentUris.withAppendedId(CONTENT_URI_PHOTOVIDEOS, rowID4);
				getContext().getContentResolver().notifyChange(_uri, null);
			}
			break;
		case SPONSORS:
			long rowID5 = sqlDB.insert(FestProDBAdapter.TABLE_SPONSORS, "", values);
			if (rowID5 > 0) {
				_uri = ContentUris.withAppendedId(CONTENT_URI_SPONSORS, rowID5);
				getContext().getContentResolver().notifyChange(_uri, null);
			}
			break;
		default: throw new SQLException("Failed to insert row into " + uri);
		}
		return _uri;
	}

	@Override
	public boolean onCreate() {
		Context context = getContext();
		FestProDBAdapter.FestProDBHelper dbHelper = new FestProDBAdapter.FestProDBHelper(
				context);
		sqlDB = dbHelper.getWritableDatabase();
		return (sqlDB != null);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

		switch (sURIMatcher.match(uri)) {
		case FILM_ID:
			queryBuilder.setTables(FestProDBAdapter.TABLE_FILMS);
			queryBuilder.appendWhere(FestProDBAdapter.FILMS_ID + "="
					+ uri.getLastPathSegment());
			break;
		case FILMS:
			queryBuilder.setTables(FestProDBAdapter.TABLE_FILMS);
			// no filter
			break;
		case SCREENING_ID:
			queryBuilder.setTables(FestProDBAdapter.TABLE_SCREENINGS);
			queryBuilder.appendWhere(FestProDBAdapter.SCREENINGS_ID + "="
					+ uri.getLastPathSegment());
			break;
		case SCREENINGS:
			queryBuilder.setTables(FestProDBAdapter.TABLE_SCREENINGS);
			// no filter
			break;
		case PERSONNEL_ID:
			queryBuilder.setTables(FestProDBAdapter.TABLE_PERSONNEL);
			queryBuilder.appendWhere(FestProDBAdapter.PERSONNEL_ID + "="
					+ uri.getLastPathSegment());
			break;
		case PERSONNEL:
			queryBuilder.setTables(FestProDBAdapter.TABLE_PERSONNEL);
			// no filter
			break;
		case PHOTOVIDEO_ID:
			queryBuilder.setTables(FestProDBAdapter.TABLE_PHOTOVIDEOS);
			queryBuilder.appendWhere(FestProDBAdapter.PHOTOVIDEOS_ID + "="
					+ uri.getLastPathSegment());
			break;
		case PHOTOVIDEOS:
			queryBuilder.setTables(FestProDBAdapter.TABLE_PHOTOVIDEOS);
			// no filter
			break;
		case SPONSOR_ID:
			queryBuilder.setTables(FestProDBAdapter.TABLE_SPONSORS);
			queryBuilder.appendWhere(FestProDBAdapter.SPONSORS_ID + "="
					+ uri.getLastPathSegment());
			break;
		case SPONSORS:
			queryBuilder.setTables(FestProDBAdapter.TABLE_SPONSORS);
			// no filter
			break;
		default:
			throw new IllegalArgumentException("Unknown URI");
		}

		Cursor cursor = queryBuilder.query(sqlDB, projection, selection,
				selectionArgs, null, null, sortOrder);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		int count = 0;
		switch (sURIMatcher.match(uri)) {
		case FILMS:
			count = sqlDB.update(FestProDBAdapter.TABLE_FILMS, values,
					selection, selectionArgs);
			break;
		case FILM_ID:
			count = sqlDB.update(
					FestProDBAdapter.TABLE_FILMS,
					values,
					FestProDBAdapter.FILMS_ID
							+ " = "
							+ uri.getPathSegments().get(1)
							+ (!TextUtils.isEmpty(selection) ? " AND ("
									+ selection + ')' : ""), selectionArgs);
			break;
		case SCREENINGS:
			count = sqlDB.update(FestProDBAdapter.TABLE_SCREENINGS, values,
					selection, selectionArgs);
			break;
		case SCREENING_ID:
			count = sqlDB.update(
					FestProDBAdapter.TABLE_SCREENINGS,
					values,
					FestProDBAdapter.SCREENINGS_ID
							+ " = "
							+ uri.getPathSegments().get(1)
							+ (!TextUtils.isEmpty(selection) ? " AND ("
									+ selection + ')' : ""), selectionArgs);
			break;
		case PERSONNEL:
			count = sqlDB.update(FestProDBAdapter.TABLE_PERSONNEL, values,
					selection, selectionArgs);
			break;
		case PERSONNEL_ID:
			count = sqlDB.update(
					FestProDBAdapter.TABLE_PERSONNEL,
					values,
					FestProDBAdapter.PERSONNEL_ID
							+ " = "
							+ uri.getPathSegments().get(1)
							+ (!TextUtils.isEmpty(selection) ? " AND ("
									+ selection + ')' : ""), selectionArgs);
			break;
		case PHOTOVIDEOS:
			count = sqlDB.update(FestProDBAdapter.TABLE_PHOTOVIDEOS, values,
					selection, selectionArgs);
			break;
		case PHOTOVIDEO_ID:
			count = sqlDB.update(
					FestProDBAdapter.TABLE_PHOTOVIDEOS,
					values,
					FestProDBAdapter.PHOTOVIDEOS_ID
							+ " = "
							+ uri.getPathSegments().get(1)
							+ (!TextUtils.isEmpty(selection) ? " AND ("
									+ selection + ')' : ""), selectionArgs);
			break;
		case SPONSORS:
			count = sqlDB.update(FestProDBAdapter.TABLE_SPONSORS, values,
					selection, selectionArgs);
			break;
		case SPONSOR_ID:
			count = sqlDB.update(
					FestProDBAdapter.TABLE_SPONSORS,
					values,
					FestProDBAdapter.SPONSORS_ID
							+ " = "
							+ uri.getPathSegments().get(1)
							+ (!TextUtils.isEmpty(selection) ? " AND ("
									+ selection + ')' : ""), selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count;
	}

}
