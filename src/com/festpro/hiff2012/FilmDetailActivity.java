package com.festpro.hiff2012;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ShareActionProvider;
import android.app.ActionBar;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;

public class FilmDetailActivity extends FragmentActivity {
	private ShareActionProvider mShareActionProvider;
	public ArrayList<String> screeningText = new ArrayList<String>();
	public ArrayList<String> screeningUrls = new ArrayList<String>();
	public ArrayList<String> myFestText;
	public ArrayList<String> myFestData = new ArrayList<String>();
	public String filmTitle;
	public String filmSlug;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar bar = getActionBar();
		if (bar != null) {
			bar.setDisplayHomeAsUpEnabled(true);
		}

		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			// If the screen is now in landscape mode, we can show the
			// dialog in-line with the list so we don't need this activity.
			finish();
			return;
		}

		if (savedInstanceState == null) {
			// During initial setup, plug in the details fragment.
			Bundle bundle = getIntent().getExtras();
			filmTitle = bundle.getString("title");
			filmSlug = bundle.getString("slug");
			FilmDetailActivity.this.setTitle(filmTitle);

			FilmDetailFragment details = new FilmDetailFragment();
			details.setArguments(bundle);
			getSupportFragmentManager().beginTransaction()
					.add(android.R.id.content, details).commit();
			
			SharedPreferences preferences = 
					PreferenceManager.getDefaultSharedPreferences(this);
			Set<String> set;
			Boolean added = null;
			String myFestID;
			
			// Load Screenings
			Cursor c;
			Uri allScreenings = Uri.parse("content://com.festpro.hiff2012.FestProDBProvider/screenings");			
			CursorLoader cursorLoader = new CursorLoader(this, 
					allScreenings, null, "film_ids LIKE '%"+bundle.getInt("index", 0)+"%'", null, "dateTime");
			c = cursorLoader.loadInBackground();

			// Add screening information to string arrays
			while (c.moveToNext()) {
				String film_id = c.getString(c.getColumnIndex(FestProDBAdapter.SCREENINGS_FILM_ID));
				String screening_id = c.getString(c.getColumnIndex(FestProDBAdapter.SCREENINGS_SCREENING_ID));
				myFestID = film_id+":"+screening_id;
				Log.i("FilmDetailActivity",myFestID);
				if ( preferences.contains("myFestArray") ) {
					set = preferences.getStringSet("myFestArray", null);
					if (set != null) {
						added = set.contains(myFestID);
					}
				}
				
				String programName = c.getString(c.getColumnIndex(FestProDBAdapter.SCREENINGS_PROGRAM_NAME));
				String date = c.getString(c.getColumnIndex(FestProDBAdapter.SCREENINGS_DATETIME));
				String dateStr = "";
				SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);
				SimpleDateFormat myFormat = new SimpleDateFormat("EEE, MMM d - h:mm a", Locale.US);
				myFormat.setTimeZone(TimeZone.getTimeZone(getString(R.string.app_timezone)));
				try {
					dateStr = myFormat.format(fromUser.parse(date));
				} catch (ParseException e) {
					e.printStackTrace();
				}
				if (added) { dateStr = "* "+dateStr; }
				if (!programName.equals("")) {
					screeningText.add(dateStr+System.getProperty("line.separator")+programName);					
				} else {
					screeningText.add(dateStr);					
				}
				screeningUrls.add(c.getString(c.getColumnIndex(FestProDBAdapter.SCREENINGS_TICKET_LINK)));
				myFestData.add(myFestID);
			}	        
			// Close out screening Cursor
			c.close();
			
			myFestText = new ArrayList<String>(screeningText);
			screeningText.add(0, this.getResources().getString(R.string.shopping_cart_text));
			screeningUrls.add(0, this.getResources().getString(R.string.shopping_cart_url));			
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.film_detail_activity, menu);

		// Locate MenuItem with ShareActionProvider
		MenuItem item = menu.findItem(R.id.detail_share_item);

		// Fetch and store ShareActionProvider
		mShareActionProvider = (ShareActionProvider) item.getActionProvider();
		mShareActionProvider.setShareHistoryFileName(ShareActionProvider.DEFAULT_SHARE_HISTORY_FILE_NAME);
		mShareActionProvider.setShareIntent(createShareIntent());
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// app icon in action bar clicked; go to previous screen
			finish();
			return true;
		case R.id.tickets_item:
			// launch a buy tickets view - for each screening
			ScreeningDialog ticketsDialogFragment = ScreeningDialog.newInstance(
					this.getResources().getString(R.string.film_detail_popup_title01), filmTitle, screeningText, screeningUrls);
			ticketsDialogFragment.show(getFragmentManager(), "dialog");
			return true;
		case R.id.detail_share_item:
			// launch a sharing view - Facebook, Twitter, Email
			return true;
		case R.id.myfest_item:
			// Toggle MyFest options - for each screening
			ScreeningDialog myfestDialogFragment = ScreeningDialog.newInstance(
					this.getResources().getString(R.string.film_detail_popup_title03), filmTitle, myFestText, myFestData);
			myfestDialogFragment.show(getFragmentManager(), "dialog");   
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	// Call to update the share intent
	private Intent createShareIntent() {
			String baseUrl = getResources().getString(R.string.base_web_url);
			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setType("text/plain");
			shareIntent.putExtra(Intent.EXTRA_TEXT, filmTitle+" "+baseUrl+"films/detail/"+filmSlug);
			return shareIntent;
	}

	public void cancelDialog() {
		//---perform steps when user clicks on Cancel---
		//Log.d("ScreeningDialog", "User clicks on Cancel");
	}


}
