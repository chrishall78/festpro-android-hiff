package com.festpro.hiff2012;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.makeramen.segmented.SegmentedRadioGroup;
import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.URLConnectionImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.viewpagerindicator.LinePageIndicator;
import com.viewpagerindicator.PageIndicator;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

@SuppressLint("SetJavaScriptEnabled")
public class FilmDetailFragment extends Fragment implements OnCheckedChangeListener {
    /**
     * Create a new instance of FilmDetailFragment, initialized to
     * show the text at 'index'.
     */
	
    ImageLoader imageLoader = ImageLoader.getInstance();	
	SegmentedRadioGroup segmentText;
    PhotoVideoPagerAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;
	//WebView videoView;
	Cursor photoVideoCursor;
	
	public static FilmDetailFragment newInstance(int index) {
    	FilmDetailFragment f = new FilmDetailFragment();

        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);

        return f;
    }

	public void onCheckedChanged(RadioGroup group, int checkedId) {
		if (group == segmentText) {
			LinearLayout detailsLayout = (LinearLayout) getView().findViewById(R.id.detailsLayout);
	        LinearLayout synopsisLayout = (LinearLayout) getView().findViewById(R.id.synopsisLayout);
	        LinearLayout screeningLayout = (LinearLayout) getView().findViewById(R.id.screeningLayout);
			
			if (checkedId == R.id.button_one) {
		    	detailsLayout.setVisibility(LinearLayout.VISIBLE);
				synopsisLayout.setVisibility(LinearLayout.GONE);
				screeningLayout.setVisibility(LinearLayout.GONE);
			} else if (checkedId == R.id.button_two) {
		    	detailsLayout.setVisibility(LinearLayout.GONE);
				synopsisLayout.setVisibility(LinearLayout.VISIBLE);
				screeningLayout.setVisibility(LinearLayout.GONE);
			} else if (checkedId == R.id.button_three) {
		    	detailsLayout.setVisibility(LinearLayout.GONE);
				synopsisLayout.setVisibility(LinearLayout.GONE);
				screeningLayout.setVisibility(LinearLayout.VISIBLE);
			}
		} 
	}
    public int getShownIndex() {
        return getArguments().getInt("index", 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        if (container == null) {
            // Don't do any of this work if the fragment's containing
            // frame doesn't exist.
        	return null;
        }

        // TODO: Implement Android 6 permissions for read/write storage

//		File cacheDir = StorageUtils.getOwnCacheDirectory(getActivity().getApplicationContext(), "UniversalImageLoader/Cache");
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity().getApplicationContext())
			.threadPoolSize(5)
        	.threadPriority(Thread.MIN_PRIORITY + 2)
        	.denyCacheImageMultipleSizesInMemory()
			.offOutOfMemoryHandling()
			.memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024)) // You can pass your own memory cache implementation
//        	.discCache(new UnlimitedDiscCache(cacheDir)) // You can pass your own disc cache implementation
//        	.discCacheFileNameGenerator(new HashCodeFileNameGenerator())
			.imageDownloader(new URLConnectionImageDownloader(5 * 1000, 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)
        	.defaultDisplayImageOptions(DisplayImageOptions.createSimple())
//        	.enableLogging()
        	.build();
		imageLoader.init(config);
        
        
        View rootView = inflater.inflate(R.layout.film_detail, container, false);
        int UI_COLOR_01 = getResources().getColor(R.color.ui_color_01);
        int UI_COLOR_02 = getResources().getColor(R.color.ui_color_02);
        int UI_COLOR_03 = getResources().getColor(R.color.ui_color_03);
        int UI_COLOR_04 = getResources().getColor(R.color.ui_color_04);
        int UI_COLOR_05 = getResources().getColor(R.color.ui_color_05);

        Context context = getActivity();
		LayoutParams defaultParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

		Cursor c;
		Uri allFilms = Uri.parse("content://com.festpro.hiff2012.FestProDBProvider/films");
		Uri allScreenings = Uri.parse("content://com.festpro.hiff2012.FestProDBProvider/screenings");
		Uri allPersonnel = Uri.parse("content://com.festpro.hiff2012.FestProDBProvider/personnel");
		Uri allSponsors = Uri.parse("content://com.festpro.hiff2012.FestProDBProvider/sponsors");        
		
        /* Photo/Video Information */        
        WindowManager wm = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // Segmented Control
        segmentText = (SegmentedRadioGroup) rootView.findViewById(R.id.segment_text);
        segmentText.setOnCheckedChangeListener(this);

        LinearLayout detailsLayout = (LinearLayout) rootView.findViewById(R.id.detailsLayout);
        LinearLayout synopsisLayout = (LinearLayout) rootView.findViewById(R.id.synopsisLayout);
        LinearLayout screeningLayout = (LinearLayout) rootView.findViewById(R.id.screeningLayout);
        
        // Load films
        CursorLoader cursorLoader = new CursorLoader(this.getActivity(), 
				allFilms, null, "film_id = "+getShownIndex(), null, null);
		c = cursorLoader.loadInBackground();
		c.moveToFirst();
		
		// Send request to log a mobile view for this film.
		String mobileUpdateUrl = getActivity().getString(R.string.base_web_url)
				+ "/films/appview/" + c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_SLUG));		
		new NetworkRequest().execute(mobileUpdateUrl);

        /* Film Information */
        TextView filmTitle = new TextView(context);
        String title = c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_TITLE_ENGLISH));
        if (!c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_TITLE_ROMANIZED)).equals("")) {
        	title = title + " ("+c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_TITLE_ROMANIZED))+")";
        }
        filmTitle.setText(title);
        filmTitle.setTextColor(UI_COLOR_01);
        filmTitle.setTypeface(null, Typeface.BOLD);
        detailsLayout.addView(filmTitle,defaultParams);

        TextView filmSection = new TextView(context);
        String section = c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_SECTION));
        filmSection.setText(section);
        filmSection.setTextColor(UI_COLOR_03);
        if (section != null && !section.equals("null") && !section.isEmpty()) {
        	detailsLayout.addView(filmSection,defaultParams);
        }
        
        TextView filmDetails = new TextView(context);
        String details = c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_COUNTRIES)) + " " + c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_YEAR)) + " | " + c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_RUNTIME)) + " min.";
        filmDetails.setText(details);
        filmDetails.setTextColor(UI_COLOR_03);
        detailsLayout.addView(filmDetails,defaultParams);

        TextView filmLanguages = new TextView(context);
        String languages = c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_LANGUAGES));
        filmLanguages.setText(languages);
        filmLanguages.setTextColor(UI_COLOR_03);
        detailsLayout.addView(filmLanguages,defaultParams);
        
        TextView filmGenres = new TextView(context);
        String genres = c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_GENRES));
        filmGenres.setText(genres);
        filmGenres.setTextColor(UI_COLOR_03);
        detailsLayout.addView(filmGenres,defaultParams);

        TextView filmPremiere = new TextView(context);
        String premiere = c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_PREMIERE_TYPE));
        if (!premiere.equals("null")) {
//        	if (!premiere.equals("Return Engagement")) {
//        		premiere = premiere + " Premiere";
//        	}
        	filmPremiere.setText(premiere);
        	filmPremiere.setTextColor(UI_COLOR_03);
        	detailsLayout.addView(filmPremiere,defaultParams);
        }
        
        WebView filmSynopsis = new WebView(context);
        String synopsis = c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_SYNOPSIS_LONG));
        String original_synopsis = c.getString(c.getColumnIndex(FestProDBAdapter.FILMS_SYNOPSIS_ORIGINAL));
        if (!original_synopsis.equals("")) {
        	synopsis = synopsis + "<br/>" + original_synopsis;
        }
        String backgroundColor = Integer.toHexString(UI_COLOR_05).substring(2);
        String textColor = Integer.toHexString(UI_COLOR_03).substring(2);
        String styles1 = "<html><head><style type=\"text/css\">BODY { background-color:#"+backgroundColor+"; color:#"+textColor+"; } </style></head><body>";
        String styles2 = "</body></html>";
        synopsis = styles1 + synopsis + styles2;
        filmSynopsis.loadDataWithBaseURL(getString(R.string.base_web_url), synopsis, "text/html", "UTF-8", null);
        synopsisLayout.addView(filmSynopsis,defaultParams);
        
        // Close out film Cursor
        c.close();


        // Load sponsors - image or text links
		cursorLoader = new CursorLoader(this.getActivity(), 
				allSponsors, null, "film_id = "+getShownIndex(), null, "sponsorDesc, sponsorName");
		c = cursorLoader.loadInBackground();

		String description = "";
		while (c.moveToNext()) {
			String currentDesc = c.getString(c.getColumnIndex(FestProDBAdapter.SPONSORS_DESC));
			if (!description.equals(currentDesc)) {
				TextView sponsoredBy = new TextView(context);
				sponsoredBy.setText(currentDesc);
				sponsoredBy.setTextColor(UI_COLOR_01);
		        sponsoredBy.setTypeface(null, Typeface.BOLD);
				sponsoredBy.setPaintFlags(sponsoredBy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
		    	sponsoredBy.setPadding(0, 15, 0, 0);
				detailsLayout.addView(sponsoredBy,defaultParams);
			}
			description = currentDesc;

			String sponsorName = c.getString(c.getColumnIndex(FestProDBAdapter.SPONSORS_NAME));
			String sponsorLogoUrl = c.getString(c.getColumnIndex(FestProDBAdapter.SPONSORS_LOGO_URL));
			final String sponsorSiteUrl = c.getString(c.getColumnIndex(FestProDBAdapter.SPONSORS_SITE_URL));

			if (sponsorLogoUrl.contains("http")) {
				// Display Image
				ImageView sponsorLogo = new ImageView(context);
		    	sponsorLogo.setPadding(0, 10, 0, 0);
		    	int sponsorLogoWidth = (int)Math.round(size.x/2.25);
				LayoutParams logoParams = new LayoutParams(sponsorLogoWidth, LayoutParams.WRAP_CONTENT);
				detailsLayout.addView(sponsorLogo, logoParams);
				DisplayImageOptions options = new DisplayImageOptions.Builder()
	        	.cacheInMemory()
//	        	.cacheOnDisc()
	        	.build();
				imageLoader.displayImage(sponsorLogoUrl, sponsorLogo, options);
				if (sponsorSiteUrl.contains("http") || sponsorSiteUrl.contains("https")) {
					sponsorLogo.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
			    			// Open Web View with this URL
							Intent webIntent = new Intent(getActivity(), WebViewActivity.class);
							webIntent.setData(Uri.parse(sponsorSiteUrl));
					        startActivity(webIntent);
						}
					});					
				}
        	} else {
        		// Display Text
				TextView sponsorNameView = new TextView(context);
				sponsorNameView.setText(sponsorName);
				sponsorNameView.setTextColor(UI_COLOR_03);
				if (sponsorSiteUrl.contains("http") || sponsorSiteUrl.contains("https")) {
					sponsorNameView.setPaintFlags(sponsorNameView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
					sponsorNameView.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
			    			// Open Web View with this URL
							Intent webIntent = new Intent(getActivity(), WebViewActivity.class);
							webIntent.setData(Uri.parse(sponsorSiteUrl));
					        startActivity(webIntent);
						}
					});
				}
				detailsLayout.addView(sponsorNameView,defaultParams);
        	}
		}
 
        // Close out sponsors Cursor
        c.close();
        

        // Load personnel
		cursorLoader = new CursorLoader(this.getActivity(), 
				allPersonnel, null, "film_id = "+getShownIndex(), null, "lastName");
		c = cursorLoader.loadInBackground();
		
		TextView castCrew = new TextView(context);
		castCrew.setText(getString(R.string.film_detail_cast_crew));
        castCrew.setTextColor(UI_COLOR_01);
        castCrew.setTypeface(null, Typeface.BOLD);
        castCrew.setPaintFlags(castCrew.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
    	castCrew.setPadding(0, 20, 0, 0);
		detailsLayout.addView(castCrew,defaultParams);

        /* Personnel Information */
        TextView personnelName = new TextView(context);
		
		TextView role1 = new TextView(context);
		role1.setText(getString(R.string.film_detail_director));
        role1.setTextColor(UI_COLOR_01);
        role1.setTypeface(null, Typeface.BOLD);
    	role1.setPadding(0, 5, 0, 0);

		String directorName = "";
		while (c.moveToNext()) {
			String personnelRole = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_ROLE));
			List<String> personnelList = new ArrayList<String>(Arrays.asList(personnelRole.split("\\s*,\\s*")));
			for (String thisRole : personnelList) {
				if (thisRole.equals("Director")) {
					String firstName = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_FIRSTNAME));
					String lastName = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_LASTNAME));
					directorName = directorName + firstName.trim() + " " + lastName.trim() + ", ";
				}
			}
		}
		if (directorName.length() >= 2) {
			directorName = directorName.substring(0, directorName.length()-2);
	        detailsLayout.addView(role1,defaultParams);

			personnelName.setText(directorName);
			personnelName.setTextColor(UI_COLOR_03);
			detailsLayout.addView(personnelName, defaultParams);
		}
		
		TextView role2 = new TextView(context);
		role2.setText(getString(R.string.film_detail_screenwriter));
        role2.setTextColor(UI_COLOR_01);
        role2.setTypeface(null, Typeface.BOLD);
    	role2.setPadding(0, 5, 0, 0);
        c.moveToPosition(-1);

		String screenwriterName = "";
        while (c.moveToNext()) {
			String personnelRole = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_ROLE));
			List<String> personnelList = new ArrayList<String>(Arrays.asList(personnelRole.split("\\s*,\\s*")));
			for (String thisRole : personnelList) {
				if (thisRole.equals("Screenwriter")) {
					String firstName = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_FIRSTNAME));
        			String lastName = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_LASTNAME));
        			screenwriterName = screenwriterName + firstName.trim() + " " + lastName.trim() + ", ";
				}
			}
        }
		if (screenwriterName.length() >= 2) {
			screenwriterName = screenwriterName.substring(0, screenwriterName.length()-2);
	        detailsLayout.addView(role2,defaultParams);

			personnelName = new TextView(context);
			personnelName.setText(screenwriterName);
			personnelName.setTextColor(UI_COLOR_03);
			detailsLayout.addView(personnelName, defaultParams);
		}
		
		TextView role3 = new TextView(context);
		role3.setText(getString(R.string.film_detail_producer));
        role3.setTextColor(UI_COLOR_01);
        role3.setTypeface(null, Typeface.BOLD);
    	role3.setPadding(0, 5, 0, 0);
        c.moveToPosition(-1);

		String producerName = "";
        while (c.moveToNext()) {
			String personnelRole = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_ROLE));
			List<String> personnelList = new ArrayList<String>(Arrays.asList(personnelRole.split("\\s*,\\s*")));
			for (String thisRole : personnelList) {
				if (thisRole.equals("Producer")) {
					String firstName = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_FIRSTNAME));
					String lastName = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_LASTNAME));
					producerName = producerName + firstName.trim() + " " + lastName.trim() + ", ";
				}
			}
        }
		if (producerName.length() >= 2) {
			producerName = producerName.substring(0, producerName.length()-2);
	        detailsLayout.addView(role3,defaultParams);

	        personnelName = new TextView(context);
			personnelName.setText(producerName);
			personnelName.setTextColor(UI_COLOR_03);
			detailsLayout.addView(personnelName, defaultParams);
		}

		TextView role4 = new TextView(context);
		role4.setText(getString(R.string.film_detail_cinematographer));
        role4.setTextColor(UI_COLOR_01);
        role4.setTypeface(null, Typeface.BOLD);
    	role4.setPadding(0, 5, 0, 0);
        c.moveToPosition(-1);

		String cintogName = "";
        while (c.moveToNext()) {
			String personnelRole = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_ROLE));
			List<String> personnelList = new ArrayList<String>(Arrays.asList(personnelRole.split("\\s*,\\s*")));
			for (String thisRole : personnelList) {
				if (thisRole.equals("Cinematographer")) {
					String firstName = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_FIRSTNAME));
        			String lastName = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_LASTNAME));
        			cintogName = cintogName + firstName.trim() + " " + lastName.trim() + ", ";
				}
			}
        }
		if (cintogName.length() >= 2) {
			cintogName = cintogName.substring(0, cintogName.length()-2);
	        detailsLayout.addView(role4,defaultParams);

			personnelName = new TextView(context);
			personnelName.setText(cintogName);
			personnelName.setTextColor(UI_COLOR_03);
			detailsLayout.addView(personnelName, defaultParams);
		}
		
		TextView role5 = new TextView(context);
		role5.setText(getString(R.string.film_detail_cast));
        role5.setTextColor(UI_COLOR_01);
        role5.setTypeface(null, Typeface.BOLD);
    	role5.setPadding(0, 5, 0, 0);
        c.moveToPosition(-1);

		String castName = "";
        while (c.moveToNext()) {
			String personnelRole = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_ROLE));
			List<String> personnelList = new ArrayList<String>(Arrays.asList(personnelRole.split("\\s*,\\s*")));
			for (String thisRole : personnelList) {
				if (thisRole.equals("Cast")) {
					String firstName = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_FIRSTNAME));
					String lastName = c.getString(c.getColumnIndex(FestProDBAdapter.PERSONNEL_LASTNAME));
					castName = castName + firstName.trim() + " " + lastName.trim() + ", ";
				}
			}
        }
		if (castName.length() >= 2) {
			castName = castName.substring(0, castName.length()-2);
	        detailsLayout.addView(role5,defaultParams);

			personnelName = new TextView(context);
			personnelName.setText(castName);
			personnelName.setTextColor(UI_COLOR_03);
			detailsLayout.addView(personnelName, defaultParams);	        
		}

        // Close out personnel Cursor
		c.close();
        

        // Load screenings
		cursorLoader = new CursorLoader(this.getActivity(), 
				allScreenings, null, "film_ids LIKE '%"+getShownIndex()+"%'", null, "dateTime");
		c = cursorLoader.loadInBackground();
		int screeningCount = 0;

        /* Screening Information */
		while (c.moveToNext()) {
			TextView screeningDate = new TextView(context);
			if (screeningCount != 0) { screeningDate.setPadding(0, 10, 0, 0); }
			String date = c.getString(c.getColumnIndex(FestProDBAdapter.SCREENINGS_DATETIME));
			String dateStr = "";
			SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);
			SimpleDateFormat myFormat = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
			myFormat.setTimeZone(TimeZone.getTimeZone(getString(R.string.app_timezone)));
			try {
				dateStr = myFormat.format(fromUser.parse(date));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Integer free = c.getInt(c.getColumnIndex(FestProDBAdapter.SCREENINGS_FREE));
			if (free.equals(1)) {
				screeningDate.setText(String.format("%s%s", dateStr, getString(R.string.film_detail_screening_free)));
			} else {
				screeningDate.setText(dateStr);
			}
	        screeningDate.setTextColor(UI_COLOR_01);
			screeningDate.setTextSize(18);
	        screeningDate.setTypeface(null, Typeface.BOLD);
	        screeningLayout.addView(screeningDate,defaultParams);

			TextView screeningTimeLoc = new TextView(context);
			myFormat = new SimpleDateFormat("h:mm a", Locale.US);
			myFormat.setTimeZone(TimeZone.getTimeZone(getString(R.string.app_timezone)));
			String timeStr = "";
			try {
				timeStr = myFormat.format(fromUser.parse(date));
			} catch (ParseException e) {
				e.printStackTrace();
			}
			String loc = c.getString(c.getColumnIndex(FestProDBAdapter.SCREENINGS_LOCATION));
			screeningTimeLoc.setText(String.format("%s - %s", timeStr, loc));
	        screeningTimeLoc.setTextColor(UI_COLOR_03);
			screeningLayout.addView(screeningTimeLoc,defaultParams);
			
			String programName = c.getString(c.getColumnIndex(FestProDBAdapter.SCREENINGS_PROGRAM_NAME));
			if (!programName.equals("")) {
				TextView programNameView = new TextView(context);
				programNameView.setText(String.format("%s%s", getString(R.string.film_detail_screening_program), programName));
				programNameView.setTextColor(UI_COLOR_03);
				screeningLayout.addView(programNameView,defaultParams);
			}
			screeningCount = screeningCount + 1;
		}
        
        // Close out screening Cursor
        c.close();

        // Initialize details content to be visible and hide the rest
        return rootView;
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);

        Context context = getActivity();
		Uri allPhotoVideo = Uri.parse("content://com.festpro.hiff2012.FestProDBProvider/photovideos");        
		
		// Load photos/videos
		CursorLoader cursorLoader = new CursorLoader(this.getActivity(), 
				allPhotoVideo, null, "film_id = "+getShownIndex(), null, "type desc, ordernum asc");
		photoVideoCursor = cursorLoader.loadInBackground();

		// Set PagerAdapter and PageIndicator
		mAdapter = new PhotoVideoPagerAdapter(photoVideoCursor,context);		
		
		// Check for empty PhotoVideoPagerAdapter
		if (mAdapter.getCount() == 0) {
			WindowManager wm = (WindowManager) context
					.getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			int imageWidth = size.x;
			//double imageHeightTemp = imageWidth * 0.57142857; // 21:12
			double imageHeightTemp = imageWidth * 0.56232427; // 16:9
			int imageHeight = (int) imageHeightTemp;

			ImageView filmThumb = (ImageView)getActivity().findViewById(R.id.default_photo);
			LayoutParams imageParams = new LayoutParams(imageWidth, imageHeight);
			filmThumb.setScaleType(ImageView.ScaleType.CENTER_CROP);
        	filmThumb.setImageDrawable(context.getResources().getDrawable(R.drawable.default_photo));
        	filmThumb.setLayoutParams(imageParams);

        	LayoutParams invisible = new LayoutParams(0, 0);
        	mPager = (ViewPager)getActivity().findViewById(R.id.pager);
        	mPager.setLayoutParams(invisible);
			mIndicator = (LinePageIndicator)getActivity().findViewById(R.id.indicator);
			((View) mIndicator).setLayoutParams(invisible);
		} else {
			ImageView filmThumb = (ImageView)getActivity().findViewById(R.id.default_photo);
        	LayoutParams invisible = new LayoutParams(0, 0);
        	filmThumb.setLayoutParams(invisible);
			
			mPager = (ViewPager)getActivity().findViewById(R.id.pager);
			mPager.setAdapter(mAdapter);
			mPager.setOffscreenPageLimit(6);
			mIndicator = (LinePageIndicator)getActivity().findViewById(R.id.indicator);
			mIndicator.setViewPager(mPager);
		}
    }
    
    @Override
    public void onStart() {
    	super.onStart();
        mAdapter.resumeWebView();

		LinearLayout detailsLayout = (LinearLayout) getView().findViewById(R.id.detailsLayout);
        LinearLayout synopsisLayout = (LinearLayout) getView().findViewById(R.id.synopsisLayout);
        LinearLayout screeningLayout = (LinearLayout) getView().findViewById(R.id.screeningLayout);
    	
    	detailsLayout.setVisibility(LinearLayout.VISIBLE);
		synopsisLayout.setVisibility(LinearLayout.GONE);
		screeningLayout.setVisibility(LinearLayout.GONE);
    }
    
    @Override
    public void onPause() {
        super.onPause();
        mAdapter.pauseWebView();
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.resumeWebView();
    }

    @Override
	public void onDestroy() {
        super.onDestroy();
        mAdapter.stopWebView();

        // Close out photo/video cursor
        if (!photoVideoCursor.isClosed()) {
            photoVideoCursor.close();
        }
    }
    
    private class NetworkRequest extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... urls) {
			StringBuilder stringBuilder = new StringBuilder();
			HttpClient client = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(urls[0]);
			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						stringBuilder.append(line);
					}
				}
//				else {
//					Log.e("NetworkRequest", "Failed to capture webpage body");
//				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}			
			return stringBuilder.toString();
		}
    	
    }
}