package com.festpro.hiff2012;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;

public class FilmsActivity extends FragmentActivity {

	public String filmTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar bar = getActionBar();
		if (bar != null) {
			bar.setDisplayHomeAsUpEnabled(true);
		}

		if (savedInstanceState == null) {
			Bundle bundle = getIntent().getExtras();
			if (bundle != null) {
				filmTitle = bundle.getString("title");
				FilmsActivity.this.setTitle(filmTitle);
				
				FilmsFragment fragment = new FilmsFragment();
				fragment.setArguments(bundle);
				getSupportFragmentManager().beginTransaction()
				.add(android.R.id.content, fragment).commit();
			} else {
				setContentView(R.layout.films_fragment);
			}
		} else {
			setContentView(R.layout.films_fragment);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    //MenuInflater inflater = getMenuInflater();
	    //inflater.inflate(R.menu.films_activity, menu);
	    return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        /*
	        case R.id.filter_item:
	        	// make this launch a filter view
	            return false;
	        */
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}