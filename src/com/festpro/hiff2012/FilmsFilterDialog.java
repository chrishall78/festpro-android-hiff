package com.festpro.hiff2012;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class FilmsFilterDialog extends DialogFragment {
    AlertDialog alertDialog;
    OnFilterSelectedListener mListener;

    // Container Activity must implement this interface
    public interface OnFilterSelectedListener {
        void filterFilmsList(DialogFragment dialog);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFilterSelectedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnFilterSelectedListener");
        }
    }
    
    static FilmsFilterDialog newInstance(ArrayList<String> countryList, ArrayList<String> languageList, ArrayList<String> genreList, ArrayList<String> sectionList, ArrayList<String> eventTypeList) {
    	FilmsFilterDialog fragment = new FilmsFilterDialog();
		Bundle args = new Bundle();
		args.putStringArrayList("countryList", countryList);
		args.putStringArrayList("languageList", languageList);
		args.putStringArrayList("genreList", genreList);
		args.putStringArrayList("sectionList", sectionList);
		args.putStringArrayList("eventTypeList", eventTypeList);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		super.onCreateDialog(savedInstanceState);
		String title = getResources().getString(R.string.filmfilter_msg_title);
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View customView = inflater.inflate(R.layout.films_filter, null);

		ArrayList<String> sectionList = getArguments().getStringArrayList("sectionList");
		ArrayList<String> countryList = getArguments().getStringArrayList("countryList");		
		ArrayList<String> languageList = getArguments().getStringArrayList("languageList");
		ArrayList<String> genreList = getArguments().getStringArrayList("genreList");
		ArrayList<String> eventTypeList = getArguments().getStringArrayList("eventTypeList");

		createSpinnerAndAdapter(customView, R.id.sectionList, sectionList);
		createSpinnerAndAdapter(customView, R.id.countryList, countryList);
		createSpinnerAndAdapter(customView, R.id.languageList, languageList);
		createSpinnerAndAdapter(customView, R.id.genreList, genreList);
		createSpinnerAndAdapter(customView, R.id.eventTypeList, eventTypeList);

		AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
		builder
			.setView(customView)
			.setTitle(title)
			.setPositiveButton(R.string.filmfilter_msg_filter, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					// Trigger filter of films -> MainActivity.java -> FilmsFragment.java
					mListener.filterFilmsList(FilmsFilterDialog.this);
		    	}
			})
			.setNegativeButton(R.string.filmfilter_msg_cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					alertDialog.cancel();
		    	}
			})
			.setIcon(R.drawable.action_settings)
			.setCancelable(true);

		alertDialog = builder.create();
        return alertDialog;
	}
	
	private void createSpinnerAndAdapter(View view, int resourceId, ArrayList<String> listType) {
		Spinner spinner = (Spinner) view.findViewById(resourceId);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, listType);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter);
	}
}
