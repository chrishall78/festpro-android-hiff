package com.festpro.hiff2012;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.URLConnectionImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class FilmsFragment extends ListFragment implements
		LoaderManager.LoaderCallbacks<Cursor> {
	public String filmIds;
	boolean mDualPane;
    int mCurCheckPosition = 0;
    ImageLoader imageLoader = ImageLoader.getInstance();
    ArrayList<String> cursorList = new ArrayList<String>();
    ArrayList<String> alphaList = new ArrayList<String>();
    ArrayList<String> countryList = new ArrayList<String>();
    ArrayList<String> languageList = new ArrayList<String>();
    ArrayList<String> genreList = new ArrayList<String>();
    ArrayList<String> sectionList = new ArrayList<String>();
    ArrayList<String> eventTypeList = new ArrayList<String>();
    Menu optionsMenu;
    
	// This is the Adapter being used to display the list's data
	SimpleCursorAdapter mAdapter;

	// These are the database columns that we will retrieve	
	static final String[] PROJECTION = new String[] { FestProDBAdapter.FILMS_ID,
			FestProDBAdapter.FILMS_TITLE_ENGLISH, FestProDBAdapter.FILMS_COUNTRIES,
			FestProDBAdapter.FILMS_TITLE_ALPHA, FestProDBAdapter.FILMS_IMAGE_URL,
			FestProDBAdapter.FILMS_IMAGE_PATH, FestProDBAdapter.FILMS_YEAR,
			FestProDBAdapter.FILMS_RUNTIME, FestProDBAdapter.FILMS_FILM_ID,
			FestProDBAdapter.FILMS_SLUG, FestProDBAdapter.FILMS_TITLE_ROMANIZED,
			FestProDBAdapter.FILMS_SECTION, FestProDBAdapter.FILMS_EVENT_TYPE,
			FestProDBAdapter.FILMS_LANGUAGES, FestProDBAdapter.FILMS_GENRES };

	// This is the select criteria
	static String SELECTION = null;

	// These are the selection arguments
	static final String[] SELECTARGS = null;
	
	// This is the order of results
	static final String ORDERBY = FestProDBAdapter.FILMS_TITLE_ALPHA + ", "+FestProDBAdapter.FILMS_TITLE_ENGLISH;

	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setHasOptionsMenu(true);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {				
		View view = inflater.inflate(R.layout.films, container, false);

	    // Test for fragment loading by itself instead of in the viewpager
	    filmIds = "";
	    if (container instanceof FrameLayout) { filmIds = "shortsProgram"; }
		
		TextView filmsFilterText = (TextView) view.findViewById(R.id.filmsFilterText);
		
		if (SELECTION != null && !SELECTION.isEmpty()) {
			if (savedInstanceState != null) {
				filmsFilterText.setVisibility(View.INVISIBLE);
			}
		} else {
			filmsFilterText.setVisibility(View.INVISIBLE);
		}
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		optionsMenu = menu;
		inflater.inflate(R.menu.films_activity, menu);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		optionsMenu = menu;
		try {
			MenuItem item = menu.findItem(R.id.filter_item);
		    if (countryList.isEmpty() || languageList.isEmpty() || genreList.isEmpty() || eventTypeList.isEmpty() || sectionList.isEmpty()) {
		    	// no items yet - disable
		        item.setEnabled(false);
		        item.getIcon().setAlpha(130);
		    } else {
		    	// we have items - enable
		        item.setEnabled(true);
		        item.getIcon().setAlpha(255);
		    }
		    // disable filter on shorts program view
		    if (!filmIds.equals("")) {
		        item.setEnabled(false);
		        item.getIcon().setAlpha(130);		    	
		    }
		} catch (NullPointerException e) {
			//e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.filter_item:            
	    		FilmsFilterDialog filterDialogFragment = FilmsFilterDialog.newInstance(countryList, languageList, genreList, sectionList, eventTypeList);
	    		filterDialogFragment.show(getActivity().getFragmentManager(), "filterDialog");
	            return true;
	    }
	    return super.onOptionsItemSelected(item);
	} 	

	@Override
	public void onResume() {
		super.onResume();
		getActivity().getSupportLoaderManager().restartLoader(0, null, this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// TODO: Implement Android 6 permissions for read/write storage

//		File cacheDir = StorageUtils.getOwnCacheDirectory(getActivity().getApplicationContext(), "UniversalImageLoader/Cache");
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity().getApplicationContext())
        	.threadPoolSize(5)
        	.threadPriority(Thread.MIN_PRIORITY + 2)
        	.denyCacheImageMultipleSizesInMemory()
        	.offOutOfMemoryHandling()
        	.memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024)) // You can pass your own memory cache implementation
//        	.discCache(new UnlimitedDiscCache(cacheDir)) // You can pass your own disc cache implementation
//        	.discCacheFileNameGenerator(new HashCodeFileNameGenerator())
        	.imageDownloader(new URLConnectionImageDownloader(5 * 1000, 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)
        	.defaultDisplayImageOptions(DisplayImageOptions.createSimple())
        	//.enableLogging()
        	.build();
		imageLoader.init(config);
		
		// Check to see if we have a frame in which to embed the details
        // fragment directly in the containing UI.
        View detailsFrame = getActivity().findViewById(R.id.filmDetails);
        mDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

        if (savedInstanceState != null) {
            // Restore last state for checked position.
            mCurCheckPosition = savedInstanceState.getInt("curChoice", 0);
        }

        if (mDualPane) {
            // In dual-pane mode, the list view highlights the selected item.
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            // Make sure our UI is in the correct state.
            showDetails(mCurCheckPosition, "", "");
        }
        
		// For the cursor adapter, specify which columns go into which views
		String[] fromColumns = { FestProDBAdapter.FILMS_IMAGE_URL, FestProDBAdapter.FILMS_TITLE_ALPHA, FestProDBAdapter.FILMS_TITLE_ENGLISH, FestProDBAdapter.FILMS_COUNTRIES };
		int[] toViews = { R.id.rowThumb, R.id.rowHeaderFilms, R.id.rowTitle, R.id.rowDetails };

		// Create an empty adapter we will use to display the loaded data.
		// We pass null for the cursor, then update it in onLoadFinished()
		//if (android.os.Build.VERSION.SDK_INT < 11) {
		//	mAdapter = new SimpleCursorAdapter(getActivity(), R.layout.films_row,
		//			null, fromColumns, toViews);
		//} else {
			mAdapter = new SimpleCursorAdapter(getActivity(), R.layout.films_row,
					null, fromColumns, toViews, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
			
			mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {				
				String lastValue = "";
				String currentValue = ""; 

				@Override
				public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
					if (columnIndex == 4) {
						// Image Thumbnail
						String imageUrl = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.FILMS_IMAGE_URL));
						if (imageUrl.equals("") || imageUrl.equals(null)) {
							imageUrl = getActivity().getString(R.string.base_default_photo);
						}
						ImageView rowThumb = (ImageView)view;
						DisplayImageOptions options = new DisplayImageOptions.Builder()
                		.cacheInMemory()
//                		.cacheOnDisc()
                		.build();
						imageLoader.displayImage(imageUrl, rowThumb, options);
						return true;
					} else if (columnIndex == 1) {
						// Film Title
						TextView rowTitle = (TextView) view;
						String filmTitle = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.FILMS_TITLE_ENGLISH));
						String originalTitle = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.FILMS_TITLE_ROMANIZED));
				        if (!originalTitle.equals("")) {
				        	filmTitle = filmTitle + " (" + originalTitle + ")";
				        }
						
						rowTitle.setText(filmTitle);
						rowTitle.setHorizontallyScrolling(true);
						return true;
					} else if (columnIndex == 2) {
						// Countries, Year & Runtime
						TextView rowDetails = (TextView) view;
						String filmDetails = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.FILMS_COUNTRIES))
								+ " " +  cursor.getString(cursor.getColumnIndex(FestProDBAdapter.FILMS_YEAR))
								+ " | " + cursor.getString(cursor.getColumnIndex(FestProDBAdapter.FILMS_RUNTIME)) + " min.";
						rowDetails.setText(filmDetails);
						rowDetails.setHorizontallyScrolling(true);
						return true;
					} else if (columnIndex == 3) {
//						boolean headerVisible = false;
//						boolean headerAlphaVisible = false;
//
//						// Films Header
//						TextView rowHeader = (TextView) view;
//						String headerAlpha = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.FILMS_TITLE_ALPHA));
//						currentValue = headerAlpha;
//
//						// Test to see if position has already been recorded as needing a header
//						for (String str:cursorList) { if (str.equals(String.valueOf(cursor.getPosition()))) { headerVisible = true; } }
//						for (String str:alphaList) { if (str.equals(String.valueOf(currentValue))) { headerAlphaVisible = true; } }
//
//						view.setVisibility(View.GONE);
//						if (! lastValue.equals(currentValue) && !headerAlphaVisible) {
//							rowHeader.setText(headerAlpha);
//							view.setVisibility(View.VISIBLE);
//							if (!headerVisible) {
//								cursorList.add(String.valueOf(cursor.getPosition()));
//								alphaList.add(currentValue);
//							}
//						}
//						if (cursor.isFirst() || headerVisible) {
//							rowHeader.setText(headerAlpha);
//							view.setVisibility(View.VISIBLE);
//							if (cursor.isFirst()) {
//								cursorList.add(String.valueOf(cursor.getPosition()));
//								alphaList.add(currentValue);
//							}
//						}
//
//						lastValue = headerAlpha;
//						return true;
					}
					return false;
				}
			});
		//}
		setListAdapter(mAdapter);
		
		//Log.e("FilmsFragment",cursorList.toString()+alphaList.toString());

		// Prepare the loader. Either re-connect with an existing one,
		// or start a new one.
//		if (SELECTION != null && !SELECTION.isEmpty()) {
//			Log.e("FilmsFragment","onActivityCreated: "+SELECTION);
//		}
		getActivity().getSupportLoaderManager().initLoader(0, null, this);
	}

	@Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("curChoice", mCurCheckPosition);
    }	
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Cursor cursor = mAdapter.getCursor();
		cursor.moveToPosition(position);
		int filmId = cursor.getInt(cursor.getColumnIndex(FestProDBAdapter.FILMS_FILM_ID));
		String filmTitle = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.FILMS_TITLE_ENGLISH));
		String filmSlug = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.FILMS_SLUG));
		showDetails(filmId, filmTitle, filmSlug);
	}

	/**
     * Helper function to show the details of a selected item, either by
     * displaying a fragment in-place in the current UI, or starting a
     * whole new activity in which it is displayed.
     */
	void showDetails(int index, String title, String slug) {
        mCurCheckPosition = index;

        if (mDualPane) {
            // We can display everything in-place with fragments, so update
            // the list to highlight the selected item and show the data.
            getListView().setItemChecked(index, true);

            // Check what fragment is currently shown, replace if needed.
            FilmDetailFragment details = (FilmDetailFragment)
                    getFragmentManager().findFragmentById(R.id.filmDetails);
            if (details == null || details.getShownIndex() != index) {
                // Make new fragment to show this selection.
                details = FilmDetailFragment.newInstance(index);

                // Execute a transaction, replacing any existing fragment
                // with this one inside the frame.
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.replace(R.id.filmDetails, details);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.commit();
            }

        } else {
            // Otherwise we need to launch a new activity to display
            // the dialog fragment with selected text.
            Intent intent = new Intent();
            intent.setClass(getActivity(), FilmDetailActivity.class);
            intent.putExtra("index", index);
            intent.putExtra("title", title);
            intent.putExtra("slug", slug);
            startActivity(intent);
        }
    }	
	
	// Called when a new Loader needs to be created
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		// Now create and return a CursorLoader that will take care of
		// creating a Cursor for the data being displayed.
		Bundle bundle = getActivity().getIntent().getExtras();
		if (bundle != null && filmIds != null && !filmIds.equals("")) {
			String filmIds = bundle.getString("filmIds");
			SELECTION = " film_id IN ("+filmIds+") ";
		} else {
			if (SELECTION != null && !SELECTION.isEmpty()) {
				if (SELECTION.substring(0, 8).equals(" film_id")) {
					SELECTION = null;
				}
			}
		}
		
//		if (SELECTION != null && !SELECTION.isEmpty()) {
//			Log.e("FilmsFragment","onCreateLoader: "+SELECTION);
//		}
		
		return new CursorLoader(getActivity(),
				FestProDBProvider.CONTENT_URI_FILMS, PROJECTION, SELECTION, SELECTARGS,
				ORDERBY);
	}

	// Called when a previously created loader has finished loading
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		// Swap the new cursor in. (The framework will take care of closing the
		// old cursor once we return.)
		mAdapter.swapCursor(data);

		// Create array of film filters for options menu dialog
		data.moveToPosition(-1);
		while (data.moveToNext()) {
			String country = data.getString(data.getColumnIndex(FestProDBAdapter.FILMS_COUNTRIES));
			String language = data.getString(data.getColumnIndex(FestProDBAdapter.FILMS_LANGUAGES));
			String genre = data.getString(data.getColumnIndex(FestProDBAdapter.FILMS_GENRES));
			String section = data.getString(data.getColumnIndex(FestProDBAdapter.FILMS_SECTION));
			String eventType = data.getString(data.getColumnIndex(FestProDBAdapter.FILMS_EVENT_TYPE));

			boolean inArray = false;
			for (String str:sectionList) { if (str.equals(section)) { inArray = true; } }
			if (!inArray && !section.equals("null")) { sectionList.add(section); }

			List<String> countries = Arrays.asList(country.split("\\s*,\\s*"));
			for (String newstr:countries) {
				inArray = false;
				for (String str:countryList) { if (str.equals(newstr)) { inArray = true; } }
				if (!inArray && !newstr.equals("null")) { countryList.add(newstr); }
			}

			List<String> languages = Arrays.asList(language.split("\\s*,\\s*"));
			for (String newstr:languages) {
				inArray = false;
				for (String str:languageList) { if (str.equals(newstr)) { inArray = true; } }
				if (!inArray && !newstr.equals("null")) { languageList.add(newstr); }
			}

			List<String> genres = Arrays.asList(genre.split("\\s*,\\s*"));
			for (String newstr:genres) {
				inArray = false;
				for (String str:genreList) { if (str.equals(newstr)) { inArray = true; } }
				if (!inArray && !newstr.equals("null")) { genreList.add(newstr); }
			}

			inArray = false;
			for (String str:eventTypeList) { if (str.equals(eventType)) { inArray = true; } }
			if (!inArray && !eventType.equals("null")) { eventTypeList.add(eventType); }
		}

		sectionList.removeAll(Arrays.asList("", null));
		countryList.removeAll(Arrays.asList("", null));
		languageList.removeAll(Arrays.asList("", null));
		genreList.removeAll(Arrays.asList("", null));
		eventTypeList.removeAll(Arrays.asList("", null));
		
		sortArrayList(sectionList);
		sortArrayList(countryList);
		sortArrayList(languageList);
		sortArrayList(genreList);
		sortArrayList(eventTypeList);

		String allValue = getString(R.string.filmfilter_msg_all);

		sectionList.remove(allValue);
		countryList.remove(allValue);
		languageList.remove(allValue);
		genreList.remove(allValue);
		eventTypeList.remove(allValue);
		
		if (!sectionList.isEmpty()) {
			if (!sectionList.get(0).equals(allValue)) {
				sectionList.add(0, allValue);
			}
		}
		if (!countryList.isEmpty()) {
			if (!countryList.get(0).equals(allValue)) {
				countryList.add(0, allValue);
			}
		}
		if (!languageList.isEmpty()) {
			if (!languageList.get(0).equals(allValue)) {
				languageList.add(0, allValue);
			}
		}
		if (!genreList.isEmpty()) {
			if (!genreList.get(0).equals(allValue)) {
				genreList.add(0, allValue);
			}
		}
		if (!eventTypeList.isEmpty()) {
			if (!eventTypeList.get(0).equals(allValue)) {
				eventTypeList.add(0, allValue);
			}
		}

		// Call this method to enable the menu item
		this.onPrepareOptionsMenu(optionsMenu);

//		if (SELECTION != null && !SELECTION.isEmpty()) {
//			Log.e("FilmsFragment","onLoadFinished: "+SELECTION);
//		}
	}

	// Called when a previously created loader is reset, making the data
	// unavailable
	public void onLoaderReset(Loader<Cursor> loader) {
		// This is called when the last Cursor provided to onLoadFinished()
		// above is about to be closed. We need to make sure we are no
		// longer using it.
		mAdapter.swapCursor(null);

//		if (SELECTION != null && !SELECTION.isEmpty()) {
//			Log.e("FilmsFragment","onLoaderReset: "+SELECTION);
//		}
	}

	private void sortArrayList(ArrayList<String> arrayList) {
		Collections.sort(arrayList, new Comparator<String>() {
			@Override
			public int compare(String lhs, String rhs) {
				return lhs.compareTo(rhs);
			}
		});		
	}

	public void filterFilmsList(/*String section,*/ String country, String language, String genre, String eventType) {
		//Log.e("FilmsFragment","section: "+section+" country: "+country+" language: "+language+" genre: "+genre+" eventtype: "+eventType);
		
		String all = getString(R.string.filmfilter_msg_all);
		String filterBuilder = "";
		TextView filmsFilterText = (TextView) getActivity().findViewById(R.id.filmsFilterText);
		
		if (/*section.equals(all) &&*/ country.equals(all) && language.equals(all) && genre.equals(all) && eventType.equals(all)) {
			// 'ALL' is selected on all filters - reset filter to show everything
			SELECTION = null;
			filmsFilterText.setText(all);
			filmsFilterText.setVisibility(View.INVISIBLE);
		} else {			
			String selBuilder = "";
//			if (!section.equals(all)) {
//				selBuilder = selBuilder + FestProDBAdapter.FILMS_SECTION + " = \"" + section + "\" AND ";
//				filterBuilder = filterBuilder + section + " | ";
//			}
			if (!country.equals(all)) {
				selBuilder = selBuilder + FestProDBAdapter.FILMS_COUNTRIES + " LIKE \"%" + country + "%\" AND ";
				filterBuilder = filterBuilder + country + " | ";
			}
			if (!language.equals(all)) {
				selBuilder = selBuilder + FestProDBAdapter.FILMS_LANGUAGES + " LIKE \"%" + language + "%\" AND ";
				filterBuilder = filterBuilder + language + " | ";
			}
			if (!genre.equals(all)) {
				selBuilder = selBuilder + FestProDBAdapter.FILMS_GENRES + " LIKE \"%" + genre + "%\" AND ";
				filterBuilder = filterBuilder + genre + " | ";
			}
			if (!eventType.equals(all)) {
				selBuilder = selBuilder + FestProDBAdapter.FILMS_EVENT_TYPE + " = \"" + eventType + "\" AND ";
				filterBuilder = filterBuilder + eventType + " | ";
			}

			// Remove last " AND " from selection
    		if (selBuilder.length() > 0) { selBuilder = selBuilder.substring(0, (selBuilder.length() - 5));}
			SELECTION = selBuilder;

//			if (!SELECTION.isEmpty()) {
//				Log.e("FilmsFragment","filterFilmsList: "+SELECTION);
//			}
			
    		if (filterBuilder.length() > 0) { filterBuilder = filterBuilder.substring(0, (filterBuilder.length() - 3)); }
			filmsFilterText.setText(filterBuilder);
			filmsFilterText.setVisibility(View.VISIBLE);
		}
		getActivity().getSupportLoaderManager().restartLoader(0, null, this);
	}
}