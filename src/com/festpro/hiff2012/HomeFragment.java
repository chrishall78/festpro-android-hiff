package com.festpro.hiff2012;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class HomeFragment extends ListFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    setHasOptionsMenu(true);
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		return inflater.inflate(R.layout.home_fragment, container, false);
    }	

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.home_activity, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.about_item:
			String url = "file:///android_asset/about.html";
			Intent aboutIntent = new Intent("com.festpro.FestProFileBrowser");
			aboutIntent.setData(Uri.parse(url));
			startActivity(aboutIntent);
			return true;
		case R.id.update_item:
			HomeUpdateDialog dialogFragment = HomeUpdateDialog.newInstance(
					this.getString(R.string.data_update_download),
					this.getString(R.string.data_update_message),
					"Update");
			dialogFragment.show(getActivity().getFragmentManager(), "dialog");
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

}
