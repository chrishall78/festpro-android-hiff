package com.festpro.hiff2012;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

public class HomeUpdateDialog extends DialogFragment {
	ProgressDialog progressDialog;
	int newFilms = 0;
	int newScreenings = 0;
	private Handler mHandler = new Handler();

	static HomeUpdateDialog newInstance(String title, String message, String updateType) {
		HomeUpdateDialog fragment = new HomeUpdateDialog();
		Bundle args = new Bundle();
		args.putString("title", title);
		args.putString("message", message);
		args.putString("type", updateType);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String title = getArguments().getString("title");
		String message = getArguments().getString("message");
		String type = getArguments().getString("type");

		progressDialog = new ProgressDialog(getActivity());
		progressDialog.setIcon(R.drawable.navigation_refresh);
		progressDialog.setTitle(title);
		progressDialog.setMessage(message);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		progressDialog.setProgress(0);
		progressDialog.setCancelable(false);
		progressDialog.setCanceledOnTouchOutside(false);
		
		// Add buttons only for update, not initial data download
		if (type.equals("Update")) {
			progressDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Update", new DialogInterface.OnClickListener() {
				// User clicked Update button
				public void onClick(DialogInterface dialog, int id) {
					// close dialog
					dialog.dismiss();

					// launch new dialog?
					HomeUpdateDialog dialogFragment = HomeUpdateDialog.newInstance(
							getActivity().getString(R.string.data_first_download),
							getActivity().getString(R.string.data_first_message),
							"FestivalUpdate");
					dialogFragment.show(getFragmentManager(), "dialog");	
				}
			});
			progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					// User clicked Cancel button - close dialog
				}
			});			
		}		
		return progressDialog;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onStart() {
		super.onStart();

		String type = getArguments().getString("type");
		String baseDataUrl = this.getString(R.string.base_data_url);
		
		// Disable "Update" button until we have checked for updates
		progressDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(false);

		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(getActivity());
		
		// On first run, and until all data has been successfully downloaded
		if (type.equals("FirstRun")) {
			// Get the festival data
			new GetAPIData().execute(baseDataUrl + "festival");

			// Attempt to download all of the film, schedule, personnel, photo
			// and video data but check first that there are no values already downloaded.
			String[] projection = { "_id" };
			Cursor filmsCursor = getActivity().getContentResolver().query(
					FestProDBProvider.CONTENT_URI_FILMS, projection, null,
					null, null);
			if (filmsCursor == null || filmsCursor.getCount() == 0) {
				new GetAPIData().execute(baseDataUrl + "films");
			}
			if (filmsCursor != null) {
				filmsCursor.close();
			}

			Cursor scheduleCursor = getActivity().getContentResolver().query(
					FestProDBProvider.CONTENT_URI_SCREENINGS, projection, null,
					null, null);
			if (scheduleCursor == null || scheduleCursor.getCount() == 0) {
				new GetAPIData().execute(baseDataUrl + "schedule");
			}
			if (scheduleCursor != null) {
				scheduleCursor.close();
			}

			Cursor personnelCursor = getActivity().getContentResolver().query(
					FestProDBProvider.CONTENT_URI_SCREENINGS, projection, null,
					null, null);
			if (personnelCursor == null || personnelCursor.getCount() == 0) {
				new GetAPIData().execute(baseDataUrl + "personnel");
			}
			if (personnelCursor != null) {
				personnelCursor.close();
			}

			Cursor photovideoCursor = getActivity().getContentResolver().query(
					FestProDBProvider.CONTENT_URI_PHOTOVIDEOS, projection,
					null, null, null);
			if (photovideoCursor == null || photovideoCursor.getCount() == 0) {
				new GetAPIData().execute(baseDataUrl + "photos");
				new GetAPIData().execute(baseDataUrl + "videos");
			}
			if (photovideoCursor != null) {
				photovideoCursor.close();
			}

			Cursor sponsorCursor = getActivity().getContentResolver().query(
					FestProDBProvider.CONTENT_URI_SPONSORS, projection,
					null, null, null);
			if (sponsorCursor == null || sponsorCursor.getCount() == 0) {
				new GetAPIData().execute(baseDataUrl + "sponsors");
			}
			if (sponsorCursor != null) {
				sponsorCursor.close();
			}
		} else if (type.equals("FestivalUpdate")) {
			
			// Remove existing entries in these database tables.
			String[] selectionArgs = new String[] { };
			String selection = "";
			int filmsDeleted, screeningsDeleted, personnelDeleted, photovideosDeleted, sponsorsDeleted = 0;
			
			filmsDeleted = getActivity().getContentResolver().delete(FestProDBProvider.CONTENT_URI_FILMS, selection, selectionArgs);
			screeningsDeleted = getActivity().getContentResolver().delete(FestProDBProvider.CONTENT_URI_SCREENINGS, selection, selectionArgs);
			personnelDeleted = getActivity().getContentResolver().delete(FestProDBProvider.CONTENT_URI_PERSONNEL, selection, selectionArgs);
			photovideosDeleted = getActivity().getContentResolver().delete(FestProDBProvider.CONTENT_URI_PHOTOVIDEOS, selection, selectionArgs);
			sponsorsDeleted = getActivity().getContentResolver().delete(FestProDBProvider.CONTENT_URI_SPONSORS, selection, selectionArgs);
			
//			if (filmsDeleted != 0) { }
//			if (screeningsDeleted != 0) { }
//			if (personnelDeleted != 0) { }
//			if (photovideosDeleted != 0) { }
//			if (sponsorsDeleted != 0) { }
			
			// Specifically reset the MyFest Array only when updating to a new festival
			// make sure it won't match any new films accidentally
			int currentDBVersion = preferences.getInt("databaseVersion", 0);
			if(currentDBVersion < FestProDBAdapter.DATABASE_VERSION) {
				if (preferences.contains("myFestArray")) {
					SharedPreferences.Editor prefsEditor = preferences.edit();
					prefsEditor.remove("myFestArray");
					prefsEditor.apply();
					
					Set<String> set = new HashSet<String>();
					prefsEditor.putStringSet("myFestArray", set);
					prefsEditor.apply();
				}
			}
			
			// Get the festival data
			new GetAPIData().execute(baseDataUrl + "festival");

			// Attempt to download all of the film, schedule, personnel, photo
			// and video data but check first that there are no values already downloaded.
			String[] projection = { "_id" };
			Cursor filmsCursor = getActivity().getContentResolver().query(
					FestProDBProvider.CONTENT_URI_FILMS, projection, null,
					null, null);
			new GetAPIData().execute(baseDataUrl + "films");
			if (filmsCursor != null) {
				filmsCursor.close();
			}

			Cursor scheduleCursor = getActivity().getContentResolver().query(
					FestProDBProvider.CONTENT_URI_SCREENINGS, projection, null,
					null, null);
			new GetAPIData().execute(baseDataUrl + "schedule");
			if (scheduleCursor != null) {
				scheduleCursor.close();
			}

			Cursor personnelCursor = getActivity().getContentResolver().query(
					FestProDBProvider.CONTENT_URI_SCREENINGS, projection, null,
					null, null);
			new GetAPIData().execute(baseDataUrl + "personnel");
			if (personnelCursor != null) {
				personnelCursor.close();
			}

			Cursor photovideoCursor = getActivity().getContentResolver().query(
					FestProDBProvider.CONTENT_URI_PHOTOVIDEOS, projection,
					null, null, null);
			new GetAPIData().execute(baseDataUrl + "photos");
			new GetAPIData().execute(baseDataUrl + "videos");
			if (photovideoCursor != null) {
				photovideoCursor.close();
			}

			Cursor sponsorCursor = getActivity().getContentResolver().query(
					FestProDBProvider.CONTENT_URI_SPONSORS, projection,
					null, null, null);
			if (sponsorCursor == null || sponsorCursor.getCount() == 0) {
				new GetAPIData().execute(baseDataUrl + "sponsors");
			}
			if (sponsorCursor != null) {
				sponsorCursor.close();
			}
		} else if (type.equals("Update")) {
			// Check for updates here
			progressDialog.setTitle(getActivity().getString(R.string.data_update_download));
			
			// If there is no last updated date, we cannot check for updates
			if (preferences.contains("lastUpdatedDate") ) {
				int lastUpdated = preferences.getInt("lastUpdatedDate", 0);
				if (lastUpdated != 0) {
					//Log.i("HomeUpdateDialog",lastUpdated+"");
					new GetAPIData().execute(baseDataUrl + "updates_since/" + Math.abs(lastUpdated));
				}
			}			
		}
	}

	private class GetAPIData extends AsyncTask<String, Integer, String> {

		public String readJSONFeed(String URL) {
			StringBuilder stringBuilder = new StringBuilder();
			HttpClient client = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(URL);
			try {
				HttpResponse response = client.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						stringBuilder.append(line);
					}
				}
//				else {
//					Log.e("JSON", "Failed to download file");
//				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return stringBuilder.toString();
		}

		protected String doInBackground(String... urls) {
			// Do network request for JSON string
			String jsonString = readJSONFeed(urls[0]);

			// Add the data to the database
			try {
				SharedPreferences preferences = PreferenceManager
						.getDefaultSharedPreferences(getActivity());

				JSONObject apiObject = new JSONObject(jsonString);
				String apiCall = apiObject.getString("api_call");
				ContentValues values2 = new ContentValues();
				if (apiCall.equals("festival")) {
					JSONObject apiData = apiObject.getJSONObject("api_data");

					SharedPreferences.Editor prefsEditor = preferences.edit();
					prefsEditor.putInt("festivalYear",
								Integer.parseInt(apiData.getString("year")));
					prefsEditor.putString("festivalName",
								apiData.getString("name"));
					prefsEditor.putString("festivalStartDate",
								apiData.getString("startdate"));
					prefsEditor.putString("festivalEndDate",
								apiData.getString("enddate"));
					prefsEditor.putString("festivalTimeZone",
								apiData.getString("timezone"));
					prefsEditor.putString("programUpdates",
								apiData.getString("updates"));
					prefsEditor.apply();
				} else if (apiCall.equals("updates_since")) {
					JSONObject apiData = apiObject.getJSONObject("api_data");
					newFilms = Integer.parseInt(apiData.getString("films"));
					newScreenings = Integer.parseInt(apiData.getString("schedule"));
				} else {
					JSONArray apiArray = apiObject.getJSONArray("api_data");
					for (int i = 0; i < apiArray.length(); i++) {
						JSONObject apiData = apiArray.getJSONObject(i);
						if (apiCall.equals("films")) {
							ContentValues values = new ContentValues();
							values.put("countries",
									apiData.getString("countries"));
							values.put("directorName",
									apiData.getString("directorName"));
							values.put("eventType",
									apiData.getString("eventType"));
							values.put("genres", apiData.getString("genres"));
							values.put("imagePath", "");
							values.put("imageUrl",
									apiData.getString("imageUrl"));
							values.put("languages",
									apiData.getString("languages"));
							values.put("premiereType",
									apiData.getString("premiereType"));
							values.put("section", apiData.getString("section"));
							values.put("slug", apiData.getString("slug"));
							values.put("synopsisShort",
									apiData.getString("synopsisShort"));
							values.put("synopsisLong",
									apiData.getString("synopsisLong"));
							values.put("synopsisOriginal",
									apiData.getString("synopsisOriginal"));
							values.put("titleAlpha",
									apiData.getString("titleAlpha"));
							String filmTitle = apiData.getString("titleEnglish");
							values.put("titleEnglish", switchTitle(filmTitle));
							values.put("titleRomanized",
									apiData.getString("titleRomanized"));
							values.put("film_id", Integer.parseInt(apiData
									.getString("film_id")));
							values.put("runtime", Integer.parseInt(apiData
									.getString("runtime")));
							values.put("year",
									Integer.parseInt(apiData.getString("year")));

							getActivity()
									.getContentResolver()
									.insert(FestProDBProvider.CONTENT_URI_FILMS,
											values);

						} else if (apiCall.equals("schedule")) {
							//ContentValues values2 = new ContentValues();
							values2.put("dateString",
									apiData.getString("dateString"));
							values2.put("dateTime",
									apiData.getString("dateTime"));
							values2.put("film_ids",
									apiData.getString("filmIds"));
							values2.put("films", apiData.getString("films"));
							values2.put("location",
									apiData.getString("location"));
							values2.put("programName",
									apiData.getString("programName"));
							values2.put("ticketLink",
									apiData.getString("ticketLink"));
							values2.put("slug",
									apiData.getString("slug"));
							values2.put("free",
									Integer.parseInt(apiData.getString("free")));
							values2.put("rush",
									Integer.parseInt(apiData.getString("rush")));
							values2.put("private", 0);
							values2.put("film_id", Integer.parseInt(apiData
									.getString("film_id")));
							values2.put("screening_id",
									Integer.parseInt(apiData
											.getString("screening_id")));
							values2.put("totalRuntime",
									Integer.parseInt(apiData
											.getString("totalRuntime")));
							getActivity()
									.getContentResolver()
									.insert(FestProDBProvider.CONTENT_URI_SCREENINGS,
											values2);
						} else if (apiCall.equals("personnel")) {
							ContentValues values3 = new ContentValues();
							values3.put("firstName",
									apiData.getString("firstName"));
							values3.put("lastName",
									apiData.getString("lastName"));
							values3.put("role", apiData.getString("role"));
							values3.put("film_id", Integer.parseInt(apiData
									.getString("film_id")));

							getActivity()
									.getContentResolver()
									.insert(FestProDBProvider.CONTENT_URI_PERSONNEL,
											values3);

						} else if (apiCall.equals("photos")) {
							ContentValues values4 = new ContentValues();
							values4.put("photoLargeUrl",
									apiData.getString("photo_large"));
							values4.put("photoSmallUrl",
									apiData.getString("photo_small"));
							values4.put("type", "photo");
							values4.put("videoService", "");
							values4.put("videoUrl", "");
							values4.put("film_id", Integer.parseInt(apiData
									.getString("film_id")));
							values4.put("ordernum", Integer.parseInt(apiData
									.getString("order")));

							getActivity()
									.getContentResolver()
									.insert(FestProDBProvider.CONTENT_URI_PHOTOVIDEOS,
											values4);

						} else if (apiCall.equals("videos")) {
							ContentValues values5 = new ContentValues();
							values5.put("photoLargeUrl", "");
							values5.put("photoSmallUrl", "");
							values5.put("type", "video");
							values5.put("videoService",
									apiData.getString("service_name"));
							values5.put("videoUrl",
									apiData.getString("url_video"));
							values5.put("film_id", Integer.parseInt(apiData
									.getString("film_id")));
							values5.put("ordernum", Integer.parseInt(apiData
									.getString("order")));

							getActivity()
									.getContentResolver()
									.insert(FestProDBProvider.CONTENT_URI_PHOTOVIDEOS,
											values5);
						} else if (apiCall.equals("sponsors")) {
							ContentValues values6 = new ContentValues();
							values6.put("sponsorDesc",  apiData.getString("description"));
							values6.put("sponsorLogoUrl", apiData.getString("url_logo"));
							values6.put("sponsorName", apiData.getString("name"));
							values6.put("sponsorSiteUrl", apiData.getString("url_website"));
							values6.put("film_id", Integer.parseInt(apiData.getString("film_id")));

							getActivity()
									.getContentResolver()
									.insert(FestProDBProvider.CONTENT_URI_SPONSORS,
											values6);
						}
						// Log.i("GETAPIDATA", "Film ID: " +
						// apiData.getString("film_id"));
					}

					// Confirm that data was actually inserted before
					// setting this to true
					int lastUpdated;
					java.util.Date date = new java.util.Date();
					lastUpdated = (int) date.getTime()/1000;
					
					SharedPreferences.Editor prefsEditor = preferences.edit();
					prefsEditor.putString("allDataDownloaded", "true");
					prefsEditor.putInt("databaseVersion",FestProDBAdapter.DATABASE_VERSION);
					prefsEditor.putInt("lastUpdatedDate", Math.abs(lastUpdated));
					prefsEditor.apply();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return urls[0];
		}

		protected void onPostExecute(String result) {			
			// Confirm results of check for updates
			SharedPreferences preferences = PreferenceManager
					.getDefaultSharedPreferences(getActivity());
			int lastUpdated = preferences.getInt("lastUpdatedDate", 0);
			//Log.e("HomeUpdateDialog",result+" | Last Updated: "+lastUpdated);
			
			String baseUrl = getActivity().getString(R.string.base_data_url);
			String filmsUrl = baseUrl + "films";
			String scheduleUrl = baseUrl + "schedule";
			String personnelUrl = baseUrl + "personnel";
			String photosUrl = baseUrl + "photos";
			String videosUrl = baseUrl + "videos";
			String sponsorsUrl = baseUrl + "sponsors";

			if (result.equals(filmsUrl) || result.equals(scheduleUrl) || result.equals(personnelUrl) || result.equals(photosUrl) || result.equals(videosUrl) || result.equals(sponsorsUrl) ) {
				publishProgress(14);
			}
			
			if (result.equals(baseUrl + "updates_since/"+lastUpdated)) {
				//Log.e("onPostExecute",newFilms+" / "+newScreenings);

				// No Updates Found
				if (newFilms == 0 && newScreenings == 0) {
					progressDialog.setMessage(getActivity().getString(R.string.data_update_message));
					mHandler.postDelayed(new Runnable() {
						public void run() {
							progressDialog.dismiss();
						}
					}, 3000);
				} else if (newFilms > 0 || newScreenings > 0) {
					progressDialog.setMessage("Updates Found: "+newFilms+" Films & "+newScreenings+" Screenings");
					progressDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(true);
				}				
			}

			// Close dialog after normal download of all data
			if (result.equals(sponsorsUrl)) {
				progressDialog.setMessage("All Data Downloaded");
				progressDialog.setProgress(100);
				progressDialog.setCancelable(true);
				progressDialog.setCanceledOnTouchOutside(true);
				mHandler.postDelayed(new Runnable() {
					public void run() {
						progressDialog.dismiss();
					}
				}, 2000);
			}
		}

		protected void onProgressUpdate(Integer... progress) {
			progressDialog.incrementProgressBy(progress[0]);
		}
	}
	
	// Switch "TITLE, THE" to "THE TITLE", etc.
	public String switchTitle(String filmTitle) {
		String newFilmTitle;
		int length = filmTitle.length();
		if (length > 5) {
			if (filmTitle.substring(length-3).equals(", A")) {
				newFilmTitle = "A ".concat(filmTitle.substring(0, length-3));
			} else if (filmTitle.substring(length-4).equals(", AN")) {
				newFilmTitle = "AN ".concat(filmTitle.substring(0, length-4));
			} else if (filmTitle.substring(length-5).equals(", THE")) {
				newFilmTitle = "THE ".concat(filmTitle.substring(0, length-5));
			} else {
				return filmTitle;
			}
		} else {
			return filmTitle;
		}
		return newFilmTitle;
	}

}
