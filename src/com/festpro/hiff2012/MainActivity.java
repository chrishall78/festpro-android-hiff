package com.festpro.hiff2012;

import java.util.HashSet;
import java.util.Set;

import android.app.ActionBar;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.ListFragment;
import android.support.v4.view.ViewPager;
import android.widget.Spinner;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener, ScheduleDateDialog.OnDateSelectedListener, FilmsFilterDialog.OnFilterSelectedListener {
	// Instance variables
	//ProgressDialog progressDialog;
	private ViewPager viewPager;
	private TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	FilmsFragment filmsFragment;
	ScheduleFragment scheduleFragment;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Initialization
		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
		viewPager.setAdapter(mAdapter);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);		
		
		// Adding Tabs
		String[] tabs = {
				this.getString(R.string.home_button_home),
				this.getString(R.string.home_button_films),
				this.getString(R.string.home_button_schedule),
				this.getString(R.string.home_button_myfest)
		};
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
		}
		
		// ViewPager onPageChangeListener Methods
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
		 
			@Override
			public void onPageSelected(int position) {
				// Upon changing the page make the respective tab selected
				actionBar.setSelectedNavigationItem(position);
				
				// Refresh the Films, Schedule and MyFest fragments when active
				switch (position) {
					case 1:
						mAdapter.startUpdate(viewPager);
						filmsFragment = (FilmsFragment) mAdapter.getFragment(position);
						mAdapter.destroyItem(viewPager, position, filmsFragment);
						mAdapter.instantiateItem(viewPager, position);
						mAdapter.finishUpdate(viewPager);
						break;
					case 2:
						mAdapter.startUpdate(viewPager);
						scheduleFragment = (ScheduleFragment) mAdapter.getFragment(position);
						mAdapter.destroyItem(viewPager, position, scheduleFragment);
						mAdapter.instantiateItem(viewPager, position);
						mAdapter.finishUpdate(viewPager);
						break;
					case 3:
						mAdapter.startUpdate(viewPager);
						ListFragment fragment = mAdapter.getFragment(position);
						mAdapter.destroyItem(viewPager, position, fragment);
						mAdapter.instantiateItem(viewPager, position);
						mAdapter.finishUpdate(viewPager);
						break;
				}
			}
		 
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
		 
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});	

		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		
		int currentDBVersion = preferences.getInt("databaseVersion", 0);

		// Attempt to download festival & film/schedule data if it has not been
		// already downloaded.
		if (!preferences.contains("festivalName") && !preferences.contains("allDataDownloaded")) {
			HomeUpdateDialog dialogFragment = HomeUpdateDialog.newInstance(
					this.getString(R.string.data_first_download),
					this.getString(R.string.data_first_message),
					"FirstRun");
			dialogFragment.show(getFragmentManager(), "dialog");
		} else if(currentDBVersion < FestProDBAdapter.DATABASE_VERSION) {
			HomeUpdateDialog dialogFragment = HomeUpdateDialog.newInstance(
					this.getString(R.string.data_first_download),
					this.getString(R.string.data_first_message),
					"FestivalUpdate");
			dialogFragment.show(getFragmentManager(), "dialog");
		}

		// Create MyFest Array if it does not already exist
		if (!preferences.contains("myFestArray")) {
			SharedPreferences.Editor prefsEditor = preferences.edit();
			Set<String> set = new HashSet<String>();
			prefsEditor.putStringSet("myFestArray", set);
			prefsEditor.apply();
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
	}

	// Options Menu Methods - Moved to individual fragments (onCreateOptionsMenu, onPrepareOptionsMenu)

	// TabListener Methods
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}

	// OnDateSelectedListener Methods
	@Override
	public void scrollToSelectedItem(int index) {
		scheduleFragment.scrollToSelectedItem(index);
	}

	// OnFilterSelectedListener Methods
	@Override
	public void filterFilmsList(DialogFragment dialog) {
		Dialog dialogView = dialog.getDialog();

		/*Spinner sectionValue = (Spinner) dialogView.findViewById(R.id.sectionList);*/
		Spinner countryValue = (Spinner) dialogView.findViewById(R.id.countryList);
		Spinner languageValue = (Spinner) dialogView.findViewById(R.id.languageList);
		Spinner genreValue = (Spinner) dialogView.findViewById(R.id.genreList);
		Spinner eventTypeValue = (Spinner) dialogView.findViewById(R.id.eventTypeList);
		
		filmsFragment.filterFilmsList(
				/*sectionValue.getSelectedItem().toString(),*/
				countryValue.getSelectedItem().toString(), 
				languageValue.getSelectedItem().toString(),
				genreValue.getSelectedItem().toString(),
				eventTypeValue.getSelectedItem().toString());
	}
}