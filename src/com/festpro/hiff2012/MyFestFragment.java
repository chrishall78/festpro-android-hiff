package com.festpro.hiff2012;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class MyFestFragment extends ListFragment implements
		LoaderManager.LoaderCallbacks<Cursor> {
	boolean mDualPane;
	int mCurCheckPosition = 0;
	List<String> cursorList = new ArrayList<String>();
	List<String> dateList = new ArrayList<String>();
	
	// This is the Adapter being used to display the list's data
	SimpleCursorAdapter mAdapter;

	// These are the Contacts rows that we will retrieve
	static final String[] PROJECTION = new String[] { FestProDBAdapter.SCREENINGS_ID,
		FestProDBAdapter.SCREENINGS_FILMS, FestProDBAdapter.SCREENINGS_LOCATION,
		FestProDBAdapter.SCREENINGS_DATETIME, FestProDBAdapter.SCREENINGS_DATESTRING,
		FestProDBAdapter.SCREENINGS_SCREENING_ID, FestProDBAdapter.SCREENINGS_PROGRAM_NAME,
		FestProDBAdapter.SCREENINGS_FILM_ID, FestProDBAdapter.SCREENINGS_FILM_IDS,
		FestProDBAdapter.SCREENINGS_SLUG };
	
	// This is the select criteria
	String SELECTION = null;
	
	static final String[] SELECTARGS = null;
	
	// This is the order of results
	static final String ORDERBY = FestProDBAdapter.SCREENINGS_DATETIME + " ASC";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.myfest, container, false);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.myfest_activity, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.myfest_date_item:
			// TODO make this launch a date view
			return true;
		}
		return super.onOptionsItemSelected(item);
	} 	

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		AdapterView.AdapterContextMenuInfo amenuInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;	    
		Cursor cursor = mAdapter.getCursor();
		cursor.moveToPosition(amenuInfo.position);

		String filmTitle;
		if (cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_PROGRAM_NAME)).equals("")) {
			filmTitle = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILMS));
		} else {
			filmTitle = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_PROGRAM_NAME));	
		}

		String screeningTime = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_DATETIME));
		SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);
		SimpleDateFormat myFormat = new SimpleDateFormat("M/d/yy", Locale.US);
		myFormat.setTimeZone(TimeZone.getTimeZone(getString(R.string.app_timezone)));
		String reformattedStr = "";
		try {
			reformattedStr = myFormat.format(fromUser.parse(screeningTime));
		} catch (ParseException e) {
			e.printStackTrace();
		}

		menu.setHeaderTitle(R.string.myfest_msg_remove);
		menu.setHeaderIcon(R.drawable.icon_myfest_add_black);
		menu.add(Menu.NONE, 0, 0, reformattedStr+" "+filmTitle);
		menu.add(Menu.NONE, 1, 1, R.string.myfest_msg_cancel);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		super.onContextItemSelected(item);
		AdapterView.AdapterContextMenuInfo amenuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
		if (item.getItemId() == 0) {
			// Remove item from MyFest array
			SharedPreferences preferences = 
					PreferenceManager.getDefaultSharedPreferences(getActivity());
			if ( preferences.contains("myFestArray") ) {
				Cursor cursor = mAdapter.getCursor();
				cursor.moveToPosition(amenuInfo.position);
				String film_id = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILM_ID));
				String screening_id = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_SCREENING_ID));

				Set<String> set;
				set = preferences.getStringSet("myFestArray", null);
				if (set != null) {
					if (set.contains(film_id+":"+screening_id)) { set.remove(film_id+":"+screening_id); }
				}

				// Remove MyFest Array from shared preferences, then add new array
				SharedPreferences.Editor localEditor = preferences.edit();
				localEditor.remove("myFestArray");
				localEditor.apply();

				localEditor.putStringSet("myFestArray", set);
				localEditor.apply();

				// Refresh list view
				if (set != null) {
					if (set.isEmpty()) {
                        SELECTION = "screening_id = 0";
                    } else {
                        String selection = "";
						for (String aSet : set) {
							List<String> match = Arrays.asList(aSet.split("\\s*:\\s*"));
							if (match.size() > 1) {
								selection = selection + "screening_id = '" + match.get(1) + "' OR ";
							}
						}
                        if (selection.length() > 0) {
                            selection = selection.substring(0, (selection.length() - 4));
                            SELECTION = selection;
                        } else {
                            SELECTION = "screening_id = 0";
                        }
                    }
				}
				getActivity().getSupportLoaderManager().restartLoader(2, null, this);	    	
			}

		} else {
			// User hit 'Cancel' or back button
			return false;
		}
		return true;
	}

	@Override
	public void onResume() {
		super.onResume();
		getActivity().getSupportLoaderManager().restartLoader(2, null, this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		registerForContextMenu(getListView());

		SharedPreferences preferences = 
				PreferenceManager.getDefaultSharedPreferences(getActivity());
		if ( preferences.contains("myFestArray") ) {
			Set<String> set = preferences.getStringSet("myFestArray", null);
			//Log.w("MyFestFragment",set.toString());
			if (set != null) {
				if (set.isEmpty()) {
                    SELECTION = "screening_id = 0";
                } else {
                    String selection = "";
					for (String aSet : set) {
						List<String> match = Arrays.asList(aSet.split("\\s*:\\s*"));
						if (match.size() > 1) {
							selection = selection + "screening_id = '" + match.get(1) + "' OR ";
						}
					}
                    if (selection.length() > 0) {
                        selection = selection.substring(0, (selection.length() - 4));
                        SELECTION = selection;
                    } else {
                        SELECTION = "screening_id = 0";
                    }
                }
			}
		}
		
		
		// Check to see if we have a frame in which to embed the details
		// fragment directly in the containing UI.
		View detailsFrame = getActivity().findViewById(R.id.filmDetails);
		mDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

		if (savedInstanceState != null) {
			// Restore last state for checked position.
			mCurCheckPosition = savedInstanceState.getInt("curChoice", 0);
		}

		if (mDualPane) {
			// In dual-pane mode, the list view highlights the selected item.
			getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			// Make sure our UI is in the correct state.
			showDetails(mCurCheckPosition, "", "");
		}

		// For the cursor adapter, specify which columns go into which views
		String[] fromColumns = { FestProDBAdapter.SCREENINGS_DATESTRING, FestProDBAdapter.SCREENINGS_FILMS, FestProDBAdapter.SCREENINGS_LOCATION,  FestProDBAdapter.SCREENINGS_DATETIME };
		int[] toViews = { R.id.rowHeaderSchedule, R.id.rowTitleSchedule, R.id.rowDetails1, R.id.rowDetails2 };

		// Create an empty adapter we will use to display the loaded data.
		// We pass null for the cursor, then update it in onLoadFinished()
		mAdapter = new SimpleCursorAdapter(getActivity(), R.layout.myfest_row,
				null, fromColumns, toViews, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

		mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {				
			int lastDateValue = 0;
			int currentDateValue = 0; 

			@Override
			public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
				if (columnIndex == 1) {
					// Film Title / Program Name
					TextView rowTitle = (TextView) view;
					String programName = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_PROGRAM_NAME));
					String filmTitle = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILMS));
					// Display film title if program name is empty
					if (programName.equals("")) {
						rowTitle.setText(filmTitle);						
					} else {
						rowTitle.setText(programName);						
					}					
					rowTitle.setHorizontallyScrolling(true);
					return true;
				} else if (columnIndex == 2) {
					// Location
					return false;
				} else if (columnIndex == 3) {
					// Screening Time
					TextView rowTime = (TextView) view;
					String screeningTime = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_DATETIME));
					
					SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);
					SimpleDateFormat myFormat = new SimpleDateFormat("EEE, MMM d h:mm a", Locale.US);
					myFormat.setTimeZone(TimeZone.getTimeZone(getString(R.string.app_timezone)));
					String reformattedStr = "";
					try {
						reformattedStr = myFormat.format(fromUser.parse(screeningTime));
					} catch (ParseException e) {
						e.printStackTrace();
					}

					rowTime.setText(reformattedStr);
					return true;
				} else if (columnIndex == 4) {
//					boolean headerVisible = false;
//					boolean headerDateVisible = false;
//
//					// Screening Time
//					TextView rowHeader = (TextView) view;
//					String screeningDate = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_DATESTRING));
//					String reformattedStr = "";
//					SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);
//					SimpleDateFormat myFormat = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
//					myFormat.setTimeZone(TimeZone.getTimeZone(getString(R.string.app_timezone)));
//					SimpleDateFormat simpleDate = new SimpleDateFormat("yyyyMMdd", Locale.US);
//					simpleDate.setTimeZone(TimeZone.getTimeZone(getString(R.string.app_timezone)));
//
//					// Set currentDateValue
//					try {
//						currentDateValue = Integer.parseInt(simpleDate.format(fromUser.parse(screeningDate)));
//					} catch (ParseException e) {
//						e.printStackTrace();
//					}
//
//					// Test to see if position has already been recorded as needing a header
//					for (String str:cursorList) { if (str.equals(String.valueOf(cursor.getPosition()))) { headerVisible = true; } }
//					for (String str:dateList) { if (str.equals(String.valueOf(currentDateValue))) { headerDateVisible = true; } }
//
//					// Hide header if this is not the first occurrence of a specific date
//					view.setVisibility(View.GONE);
//
//					if (lastDateValue < currentDateValue && !headerDateVisible) {
//						try {
//							reformattedStr = myFormat.format(fromUser.parse(screeningDate));
//						} catch (ParseException e) {
//							e.printStackTrace();
//						}
//						rowHeader.setText(reformattedStr);
//						view.setVisibility(View.VISIBLE);
//						if (!headerVisible) {
//							cursorList.add(String.valueOf(cursor.getPosition()));
//							dateList.add(String.valueOf(currentDateValue));
//						}
//					}
//
//					if (cursor.isFirst() || headerVisible) {
//							try {
//								reformattedStr = myFormat.format(fromUser.parse(screeningDate));
//							} catch (ParseException e) {
//								e.printStackTrace();
//							}
//							rowHeader.setText(reformattedStr);
//							view.setVisibility(View.VISIBLE);
//							if (cursor.isFirst()) {
//								cursorList.add(String.valueOf(cursor.getPosition()));
//								dateList.add(String.valueOf(currentDateValue));
//							}
//						}
//
//					// Set lastDateValue
//					try {
//						lastDateValue = Integer.parseInt(simpleDate.format(fromUser.parse(screeningDate)));
//					} catch (ParseException e) {
//						e.printStackTrace();
//					}
//
					return true;
				}
				return false;
			}
		});
		
		setListAdapter(mAdapter);

		// Prepare the loader. Either re-connect with an existing one,
		// or start a new one.
		getActivity().getSupportLoaderManager().initLoader(2, null, this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("curChoice", mCurCheckPosition);
	}	
	
	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		Cursor cursor = mAdapter.getCursor();
		cursor.moveToPosition(position);
		int filmId = cursor.getInt(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILM_ID));
		String filmIds = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILM_IDS));
		String filmSlug = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_SLUG));
		String filmTitle;
		if (cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_PROGRAM_NAME)).equals("")) {
			filmTitle = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILMS));
			showDetails(filmId, filmTitle, filmSlug);
		} else {
			filmTitle = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_PROGRAM_NAME));	
			showShortsProgram(filmId, filmIds, filmTitle);
		}
	}

	void showShortsProgram(int index, String filmIds, String title) {
		mCurCheckPosition = index;

		if (mDualPane) {
			// We can display everything in-place with fragments, so update
			// the list to highlight the selected item and show the data.
			getListView().setItemChecked(index, true);

			// Check what fragment is currently shown, replace if needed.
			FilmDetailFragment details = (FilmDetailFragment)
					getFragmentManager().findFragmentById(R.id.filmDetails);
			if (details == null || details.getShownIndex() != index) {
				// Make new fragment to show this selection.
				details = FilmDetailFragment.newInstance(index);

				// Execute a transaction, replacing any existing fragment
				// with this one inside the frame.
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.filmDetails, details);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
			}

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			Intent intent = new Intent();
			intent.setClass(getActivity(), FilmsActivity.class);
			intent.putExtra("index", index);
			intent.putExtra("filmIds", filmIds);
			intent.putExtra("title", title);
			startActivity(intent);
		}
	}

	/**
	 * Helper function to show the details of a selected item, either by
	 * displaying a fragment in-place in the current UI, or starting a
	 * whole new activity in which it is displayed.
	 */
	void showDetails(int index, String title, String slug) {
		mCurCheckPosition = index;

		if (mDualPane) {
			// We can display everything in-place with fragments, so update
			// the list to highlight the selected item and show the data.
			getListView().setItemChecked(index, true);

			// Check what fragment is currently shown, replace if needed.
			FilmDetailFragment details = (FilmDetailFragment)
					getFragmentManager().findFragmentById(R.id.filmDetails);
			if (details == null || details.getShownIndex() != index) {
				// Make new fragment to show this selection.
				details = FilmDetailFragment.newInstance(index);

				// Execute a transaction, replacing any existing fragment
				// with this one inside the frame.
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.filmDetails, details);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
			}

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			Intent intent = new Intent();
			intent.setClass(getActivity(), FilmDetailActivity.class);
			intent.putExtra("index", index);
			intent.putExtra("title", title);
			intent.putExtra("slug", slug);
			startActivity(intent);
		}
	}	

	// Called when a new Loader needs to be created
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		// Now create and return a CursorLoader that will take care of
		// creating a Cursor for the data being displayed.
		return new CursorLoader(getActivity(),
				FestProDBProvider.CONTENT_URI_SCREENINGS, PROJECTION, SELECTION, SELECTARGS,
				ORDERBY);
	}

	// Called when a previously created loader has finished loading
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		// Swap the new cursor in. (The framework will take care of closing the
		// old cursor once we return.)
		mAdapter.swapCursor(data);


		// Checks for MyFest entries with old "_id" values and updates them to "screening_id" values
		// Can be removed after next "major" festival update.
		SharedPreferences preferences =
				PreferenceManager.getDefaultSharedPreferences(getActivity());
		if ( preferences.contains("myFestArray") ) {
			Set<String> myNewSet = new HashSet<String>();
			Set<String> myFestSet = preferences.getStringSet("myFestArray", null);

			Iterator<String> iter = null;
			if (myFestSet != null) {
				iter = myFestSet.iterator();
			}
			if (iter != null) {
				while (iter.hasNext()) {
                    String currentId = iter.next();
                    data.moveToPosition(-1);
                    while (data.moveToNext()) {
                        String newScreeningId = data.getString(data.getColumnIndex(FestProDBAdapter.SCREENINGS_SCREENING_ID));
                        String filmIds = data.getString(data.getColumnIndex(FestProDBAdapter.SCREENINGS_FILM_IDS));
                        List<String> filmIdArray = Arrays.asList(filmIds.split("\\s*,\\s*"));
                        // Loop through existing myFest entries and match up eventival film ids
                        for(String eventivalFilmId:filmIdArray) {
                            for(String screening_id:myFestSet) {
                                List<String> match = Arrays.asList(filmIds.split("\\s*:\\s*"));
                                if (eventivalFilmId.equals(match.get(0))) {
                                    myNewSet.add(eventivalFilmId+":"+newScreeningId);
                                }
                            }
                        }
                    }
                }
			}

			if (!myNewSet.isEmpty()) {
				// Remove MyFest Array from shared preferences, then add new array
				SharedPreferences.Editor localEditor = preferences.edit();
				localEditor.remove("myFestArray");
				localEditor.putStringSet("myFestArray", myNewSet);
				localEditor.apply();
				if (!localEditor.commit()) {
					//Log.i("MyFestFragment", "MyFest update failed.");
				}
			}
		}
	}

	// Called when a previously created loader is reset, making the data
	// unavailable
	public void onLoaderReset(Loader<Cursor> loader) {
		// This is called when the last Cursor provided to onLoadFinished()
		// above is about to be closed. We need to make sure we are no
		// longer using it.
		mAdapter.swapCursor(null);
	}
}