package com.festpro.hiff2012;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.itri.html5webview.HTML5WebView;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.UsingFreqLimitedMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.download.URLConnectionImageDownloader;
import com.nostra13.universalimageloader.utils.StorageUtils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Point;
import android.support.v4.view.PagerAdapter;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;

public class PhotoVideoPagerAdapter extends PagerAdapter {
    private ImageLoader imageLoader = ImageLoader.getInstance();	
	private Cursor cursor;
	private Context context;
	public HTML5WebView videoView = null;

	public PhotoVideoPagerAdapter(Cursor c, Context context){
        this.cursor = c;
        this.context = context;

		// TODO: Implement Android 6 permissions for read/write storage

//		File cacheDir = StorageUtils.getOwnCacheDirectory(context.getApplicationContext(), "UniversalImageLoader/Cache");
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context.getApplicationContext())
        	.threadPoolSize(5)
        	.threadPriority(Thread.MIN_PRIORITY + 2)
        	.denyCacheImageMultipleSizesInMemory()
        	.offOutOfMemoryHandling()
        	.memoryCache(new UsingFreqLimitedMemoryCache(2 * 1024 * 1024)) // You can pass your own memory cache implementation
//        	.discCache(new UnlimitedDiscCache(cacheDir)) // You can pass your own disc cache implementation
//        	.discCacheFileNameGenerator(new HashCodeFileNameGenerator())
        	.imageDownloader(new URLConnectionImageDownloader(5 * 1000, 30 * 1000)) // connectTimeout (5 s), readTimeout (30 s)
        	.defaultDisplayImageOptions(DisplayImageOptions.createSimple())
//        	.enableLogging()
        	.build();
		imageLoader.init(config);	
	}
	
	@Override
	public int getCount() {
		return cursor.getCount();
	}
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public Object instantiateItem(ViewGroup collection, int position) {
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);

		int imageWidth = size.x;
		//double imageHeightTemp = imageWidth * 0.57142857; // 21:12
		double imageHeightTemp = imageWidth * 0.56232427; // 16:9
		int imageHeight = (int) imageHeightTemp;		
		int videoColIndex = cursor
				.getColumnIndex(FestProDBAdapter.PHOTOVIDEOS_VIDEO_URL);
		int serviceColIndex = cursor
				.getColumnIndex(FestProDBAdapter.PHOTOVIDEOS_VIDEO_SERVICE);
		int photoColIndex = cursor
				.getColumnIndex(FestProDBAdapter.PHOTOVIDEOS_PHOTO_LARGE_URL);

		cursor.moveToPosition(position);

		Boolean validVideoSource = false;
		String imageUrl = cursor.getString(photoColIndex);
		String videoUrl = cursor.getString(videoColIndex);
		String[] urlString1;
		String[] urlString2;
		String videoID;
		if (cursor.getString(serviceColIndex).equals("youtube")) {
			urlString1 = videoUrl.split("v=");
			if (urlString1.length >= 2) {
				urlString2 = urlString1[1].split("&");
				videoID = urlString2[0];
				videoUrl = "http://www.youtube.com/embed/" + videoID;
			}
			validVideoSource = true;
		} else if (cursor.getString(serviceColIndex).equals("vimeo")) {
			urlString1 = videoUrl.split("/");
			videoID = urlString1[urlString1.length - 1];
			videoUrl = "http://player.vimeo.com/video/" + videoID;
			validVideoSource = true;
		}

		if (!videoUrl.isEmpty() && validVideoSource) {
			videoView = new HTML5WebView(context);
			LayoutParams videoParams = new LayoutParams(imageWidth, imageHeight);
			videoView.loadUrl(videoUrl);

			collection.addView(videoView, videoParams);
			return videoView;
		} else if (!imageUrl.isEmpty()) {
			ImageView filmThumb = new ImageView(context);
			LayoutParams imageParams = new LayoutParams(imageWidth, imageHeight);
			
			filmThumb.setScaleType(ImageView.ScaleType.CENTER_CROP);
        	filmThumb.setImageDrawable(context.getResources().getDrawable(R.drawable.default_photo));
        	filmThumb.setTag(imageUrl);
        	collection.addView(filmThumb, imageParams);

			DisplayImageOptions options = new DisplayImageOptions.Builder()
					.cacheInMemory()
//					.cacheOnDisc()
					.build();
			imageLoader.displayImage(imageUrl, filmThumb, options);

			return filmThumb;
		} else if (!videoUrl.isEmpty() && !validVideoSource) {
			// we have a video url but it is not a valid source. don't instantiate
			// this item since we don't want a default photo before real ones
			//return false;
		}

		ImageView filmThumb = new ImageView(context);
		LayoutParams imageParams = new LayoutParams(imageWidth, imageHeight);
		String defaultImageUrl = context.getResources().getString(R.string.base_default_photo);
		
		filmThumb.setScaleType(ImageView.ScaleType.CENTER_CROP);
    	filmThumb.setImageDrawable(context.getResources().getDrawable(R.drawable.default_photo));
    	filmThumb.setTag(defaultImageUrl);
    	collection.addView(filmThumb, imageParams);

		return filmThumb;
	}

	@Override  
	public void destroyItem(ViewGroup collection, int position, Object view) {
	}
	
	@Override
	public boolean isViewFromObject(View view, Object object) {
		//Log.e("PVPA","View: "+view.toString()+" Object: "+object.toString());
		return (view.equals(object));
	}
		
	public void pauseWebView() {
        if (videoView != null) {
        	try {
        		Class.forName("android.webkit.WebView")
            		.getMethod("onPause", (Class[]) null)
                		.invoke(videoView, (Object[]) null);
        	} catch(ClassNotFoundException cnfe) {
        		cnfe.printStackTrace();
        	} catch(NoSuchMethodException nsme) {
        		nsme.printStackTrace();
        	} catch(InvocationTargetException ite) {
            	ite.printStackTrace();
        	} catch (IllegalAccessException iae) {
        		iae.printStackTrace();
        	}
        	videoView.pauseTimers();
        }		
	}
	
	public void resumeWebView() {
        if (videoView != null) {
        	try {
        		Class.forName("android.webkit.WebView")
            		.getMethod("onResume", (Class[]) null)
            			.invoke(videoView, (Object[]) null);
        	} catch(ClassNotFoundException cnfe) {
        		cnfe.printStackTrace();
        	} catch(NoSuchMethodException nsme) {
        		nsme.printStackTrace();
        	} catch(InvocationTargetException ite) {
            	ite.printStackTrace();
        	} catch (IllegalAccessException iae) {
        		iae.printStackTrace();
        	}
        	videoView.resumeTimers();
        } else {
        	WebView sampleWebView = new WebView(context);
        	sampleWebView.resumeTimers();
        }
	}
	
	public void stopWebView() {
        // Stopping a webview and all of the background processes (flash,
        // javascript, etc) is a very big mess.
        // The following steps are to counter most of the issues seen on
        // internals still going on after the webview is destroyed.
        if (videoView != null) {
        	videoView.stopLoading();
        	videoView.loadData("", "text/html", "utf-8");
        	videoView.reload();

        	videoView.setWebChromeClient(null);
        	videoView.setWebViewClient(null);

        	videoView.removeAllViews();
        	videoView.clearHistory();
        	videoView.destroy();
        	videoView = null;
        }		
	}
}
