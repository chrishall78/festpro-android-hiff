package com.festpro.hiff2012;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

public class ScheduleDateDialog extends DialogFragment {
	AlertDialog alertDialog;
	OnDateSelectedListener mListener;

	// Container Activity must implement this interface
	public interface OnDateSelectedListener {
		void scrollToSelectedItem(int index);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (OnDateSelectedListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement OnDateSelectedListener");
		}
	}
	
	static ScheduleDateDialog newInstance(ArrayList<String> itemText) {
		ScheduleDateDialog fragment = new ScheduleDateDialog();
		Bundle args = new Bundle();
		args.putStringArrayList("itemText", itemText);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		String title = getResources().getString(R.string.menu_schedule_date);
		ArrayList<String> arrText = getArguments().getStringArrayList("itemText");
		String[] itemText = new String[0];
		if (arrText != null) {
			itemText = arrText.toArray(new String[arrText.size()]);
		}

		AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
		alertDialog = builder
			.setTitle(title)
			.setItems(itemText, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					// Make fragment scroll to the appropriate header -> MainActivity.java -> ScheduleFragment.java
					mListener.scrollToSelectedItem(item);
				}
			})
			.setNegativeButton(R.string.schedule_msg_cancel, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int item) {
					alertDialog.cancel();
				}
			})
			.setIcon(R.drawable.collections_go_to_today)
			.setCancelable(true)
			.create();
		return alertDialog;
	}
}
