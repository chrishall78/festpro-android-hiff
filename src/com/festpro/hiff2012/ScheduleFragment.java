package com.festpro.hiff2012;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class ScheduleFragment extends ListFragment  implements
		LoaderManager.LoaderCallbacks<Cursor>  {
	boolean mDualPane;
	int mCurCheckPosition = 0;
	ArrayList<String> cursorList = new ArrayList<String>();
	ArrayList<String> dateList = new ArrayList<String>();
	ArrayList<String> dateDialogList = new ArrayList<String>();
	ArrayList<String> dateDialogData = new ArrayList<String>();
	Menu optionsMenu;

	// This is the Adapter being used to display the list's data
	ScheduleSimpleCursorAdapter mAdapter;

	// These are the Contacts rows that we will retrieve
	static final String[] PROJECTION = new String[] { FestProDBAdapter.SCREENINGS_ID,
		FestProDBAdapter.SCREENINGS_FILMS, FestProDBAdapter.SCREENINGS_LOCATION,
		FestProDBAdapter.SCREENINGS_DATETIME, FestProDBAdapter.SCREENINGS_DATESTRING,
		FestProDBAdapter.SCREENINGS_SCREENING_ID, FestProDBAdapter.SCREENINGS_PROGRAM_NAME,
		FestProDBAdapter.SCREENINGS_FILM_ID, FestProDBAdapter.SCREENINGS_FILM_IDS,
		FestProDBAdapter.SCREENINGS_SLUG };
	
	// This is the select criteria
	static final String SELECTION = null;

	// This is the selection arguments
	static final String[] SELECTARGS = null;

	// This is the order of results
	static final String ORDERBY = FestProDBAdapter.SCREENINGS_DATETIME + " ASC";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.schedule, container, false);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		optionsMenu = menu;
		inflater.inflate(R.menu.schedule_activity, menu);
	}

	@Override
	public void onPrepareOptionsMenu (Menu menu) {
		super.onPrepareOptionsMenu(menu);
		optionsMenu = menu;
		try {
			MenuItem item = menu.findItem(R.id.schedule_date_item);
			if (dateDialogList.isEmpty()) {
				// no items yet - disable
				item.setEnabled(false);
				item.getIcon().setAlpha(130);
			} else {
				// we have items - enable
				item.setEnabled(true);
				item.getIcon().setAlpha(255);
			}
		} catch (NullPointerException e) {
			//e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.schedule_date_item:
				ScheduleDateDialog dateDialogFragment = ScheduleDateDialog.newInstance(dateDialogList);
				dateDialogFragment.show(getActivity().getFragmentManager(), "dateDialog");
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onStart() {
		super.onStart();

	}

	@Override
	public void onResume() {
		super.onResume();
		getActivity().getSupportLoaderManager().restartLoader(1, null, this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Check to see if we have a frame in which to embed the details
		// fragment directly in the containing UI.
		View detailsFrame = getActivity().findViewById(R.id.filmDetails);
		mDualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

		if (savedInstanceState != null) {
			// Restore last state for checked position.
			mCurCheckPosition = savedInstanceState.getInt("curChoice", 0);
		}

		if (mDualPane) {
			// In dual-pane mode, the list view highlights the selected item.
			getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
			// Make sure our UI is in the correct state.
			showDetails(mCurCheckPosition, "", "");
		}

		// For the cursor adapter, specify which columns go into which views
		String[] fromColumns = { FestProDBAdapter.SCREENINGS_DATESTRING, FestProDBAdapter.SCREENINGS_FILMS, FestProDBAdapter.SCREENINGS_LOCATION,  FestProDBAdapter.SCREENINGS_DATETIME, FestProDBAdapter.SCREENINGS_SCREENING_ID };
		int[] toViews = { R.id.rowHeaderSchedule, R.id.rowTitleSchedule, R.id.rowDetails1, R.id.rowDetails2, R.id.rowThumbSchedule };
		

		// Create an empty adapter we will use to display the loaded data.
		// We pass null for the cursor, then update it in onLoadFinished()
		mAdapter = new ScheduleSimpleCursorAdapter(getActivity(), R.layout.schedule_row,
				null, fromColumns, toViews, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

		mAdapter.setViewBinder(new ScheduleSimpleCursorAdapter.ViewBinder() {
			int lastDateValue = 0;
			int currentDateValue = 0; 
			
			@Override
			public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
				if (columnIndex == 5) {
					ImageView imageView = (ImageView) view;
					SharedPreferences preferences = 
							PreferenceManager.getDefaultSharedPreferences(getActivity());
					if ( preferences.contains("myFestArray") ) {
						String film_id = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILM_ID));
						String screening_id = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_SCREENING_ID));
						//Log.w("ScheduleFragment",film_id+":"+screening_id);
						Set<String> set;
						set = preferences.getStringSet("myFestArray", null);

						if (set != null) {
							if (set.contains(film_id+":"+screening_id)) {
                                imageView.setImageResource(R.drawable.icon_myfest_remove);
                            } else {
                                imageView.setImageResource(R.drawable.icon_myfest_add_black);
                            }
						}
					}
					return true;
				} else if (columnIndex == 1) {
					// Film Title / Program Name
					TextView rowTitle = (TextView) view;
					String programName = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_PROGRAM_NAME));
					String filmTitle = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILMS));
					// Display film title if program name is empty
					if (programName.equals("")) {
						rowTitle.setText(filmTitle);						
					} else {
						rowTitle.setText(programName);						
					}					
					rowTitle.setHorizontallyScrolling(true);
					return true;
				} else if (columnIndex == 2) {
					// Location
					return false;
				} else if (columnIndex == 3) {
					// Screening Time
					TextView rowTime = (TextView) view;
					String screeningTime = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_DATETIME));
					SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);
					SimpleDateFormat myFormat = new SimpleDateFormat("EEE, MMM d h:mm a", Locale.US);
					myFormat.setTimeZone(TimeZone.getTimeZone(getString(R.string.app_timezone)));
					String reformattedStr = "";
					try {
						reformattedStr = myFormat.format(fromUser.parse(screeningTime));
					} catch (ParseException e) {
						//e.printStackTrace();
					}

					rowTime.setText(reformattedStr);
					return true;
				} else if (columnIndex == 4) {
//					boolean headerVisible = false;
//					boolean headerDateVisible = false;
//
//					// Screening Time
//					TextView rowHeader = (TextView) view;
//					String reformattedStr = "";
//					String screeningDate = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_DATESTRING));
//					SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);
//					SimpleDateFormat myFormat = new SimpleDateFormat("EEEE, MMMM d, yyyy", Locale.US);
//					myFormat.setTimeZone(TimeZone.getTimeZone(getString(R.string.app_timezone)));
//					SimpleDateFormat simpleDate = new SimpleDateFormat("yyyyMMdd", Locale.US);
//					simpleDate.setTimeZone(TimeZone.getTimeZone(getString(R.string.app_timezone)));
//
//					// Set currentDateValue
//					try {
//						currentDateValue = Integer.parseInt(simpleDate.format(fromUser.parse(screeningDate)));
//					} catch (ParseException e) {
//						//e.printStackTrace();
//					}
//
//					// Test to see if position has already been recorded as needing a header
//					for (String str:cursorList) { if (str.equals(String.valueOf(cursor.getPosition()))) { headerVisible = true; } }
//					for (String str:dateList) { if (str.equals(String.valueOf(currentDateValue))) { headerDateVisible = true; } }
//
//					// Hide header if this is not the first occurrence of a specific date
//					view.setVisibility(View.GONE);
//
//					if (lastDateValue < currentDateValue && !headerDateVisible) {
//						try {
//							reformattedStr = myFormat.format(fromUser.parse(screeningDate));
//						} catch (ParseException e) {
//							//e.printStackTrace();
//						}
//						rowHeader.setText(reformattedStr);
//						view.setVisibility(View.VISIBLE);
//						if (!headerVisible) {
//							cursorList.add(String.valueOf(cursor.getPosition()));
//							dateList.add(String.valueOf(currentDateValue));
//						}
//					}
//
//					if (cursor.isFirst() || headerVisible) {
//						try {
//							reformattedStr = myFormat.format(fromUser.parse(screeningDate));
//						} catch (ParseException e) {
//							//e.printStackTrace();
//						}
//						rowHeader.setText(reformattedStr);
//						view.setVisibility(View.VISIBLE);
//						if (cursor.isFirst()) {
//							cursorList.add(String.valueOf(cursor.getPosition()));
//							dateList.add(String.valueOf(currentDateValue));
//						}
//					}
//
//					// Set lastDateValue
//					try {
//						lastDateValue = Integer.parseInt(simpleDate.format(fromUser.parse(screeningDate)));
//					} catch (ParseException e) {
//						//e.printStackTrace();
//					}
//
					return true;
				}
				return false;
			}
		});
		
		setListAdapter(mAdapter);
		
		// Prepare the loader. Either re-connect with an existing one,
		// or start a new one.
		getActivity().getSupportLoaderManager().initLoader(1, null, this);	
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("curChoice", mCurCheckPosition);
	}	

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {		
		Cursor cursor = mAdapter.getCursor();
		cursor.moveToPosition(position);
		int filmId = cursor.getInt(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILM_ID));
		String filmIds = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILM_IDS));
		String filmSlug = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_SLUG));
		String filmTitle;
		if (cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_PROGRAM_NAME)).equals("")) {
			filmTitle = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILMS));
			showDetails(filmId, filmTitle, filmSlug);
		} else {
			filmTitle = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_PROGRAM_NAME));	
			showShortsProgram(filmId, filmIds, filmTitle);
		}
	}

	void showShortsProgram(int index, String filmIds, String title) {
		mCurCheckPosition = index;

		if (mDualPane) {
			// We can display everything in-place with fragments, so update
			// the list to highlight the selected item and show the data.
			getListView().setItemChecked(index, true);

			// Check what fragment is currently shown, replace if needed.
			FilmDetailFragment details = (FilmDetailFragment)
					getFragmentManager().findFragmentById(R.id.filmDetails);
			if (details == null || details.getShownIndex() != index) {
				// Make new fragment to show this selection.
				details = FilmDetailFragment.newInstance(index);

				// Execute a transaction, replacing any existing fragment
				// with this one inside the frame.
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.filmDetails, details);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
			}

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			Intent intent = new Intent();
			intent.setClass(getActivity(), FilmsActivity.class);
			intent.putExtra("index", index);
			intent.putExtra("filmIds", filmIds);
			intent.putExtra("title", title);
			startActivity(intent);
		}
	}

	/**
	 * Helper function to show the details of a selected item, either by
	 * displaying a fragment in-place in the current UI, or starting a
	 * whole new activity in which it is displayed.
	 */
	void showDetails(int index, String title, String slug) {
		mCurCheckPosition = index;

		if (mDualPane) {
			// We can display everything in-place with fragments, so update
			// the list to highlight the selected item and show the data.
			getListView().setItemChecked(index, true);

			// Check what fragment is currently shown, replace if needed.
			FilmDetailFragment details = (FilmDetailFragment)
					getFragmentManager().findFragmentById(R.id.filmDetails);
			if (details == null || details.getShownIndex() != index) {
				// Make new fragment to show this selection.
				details = FilmDetailFragment.newInstance(index);

				// Execute a transaction, replacing any existing fragment
				// with this one inside the frame.
				FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.replace(R.id.filmDetails, details);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
			}

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			Intent intent = new Intent();
			intent.setClass(getActivity(), FilmDetailActivity.class);
			intent.putExtra("index", index);
			intent.putExtra("title", title);
			intent.putExtra("slug", slug);
			startActivity(intent);
		}
	}		
	
	// Called when a new Loader needs to be created
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		// Now create and return a CursorLoader that will take care of
		// creating a Cursor for the data being displayed.
		return new CursorLoader(getActivity(),
				FestProDBProvider.CONTENT_URI_SCREENINGS, PROJECTION, SELECTION, SELECTARGS,
				ORDERBY);
	}

	// Called when a previously created loader has finished loading
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		// Swap the new cursor in. (The framework will take care of closing the
		// old cursor once we return.)
		mAdapter.swapCursor(data);

		// Create array of dates for options menu dialog
		int counter = 0;
		data.moveToPosition(-1);
		while (data.moveToNext()) {
			String dateTime = data.getString(data.getColumnIndex(FestProDBAdapter.SCREENINGS_DATESTRING));
			String formattedDate = "";
			boolean inArray = false;

			SimpleDateFormat fromUser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z", Locale.US);
			SimpleDateFormat myFormat = new SimpleDateFormat("EEE, MMM d, yyyy", Locale.US);
			myFormat.setTimeZone(TimeZone.getTimeZone(getString(R.string.app_timezone)));
			try {
				formattedDate = myFormat.format(fromUser.parse(dateTime));
			} catch (ParseException e) {
				//e.printStackTrace();
			}
			for (String str:dateDialogList) { if (str.equals(formattedDate)) { inArray = true; } }
			if (!inArray) {
				dateDialogList.add(formattedDate);
				dateDialogData.add(counter+"");
			}
			counter++;
		}
		// Call this method to enable the menu item
		this.onPrepareOptionsMenu(optionsMenu);
	}

	// Called when a previously created loader is reset, making the data
	// unavailable
	public void onLoaderReset(Loader<Cursor> loader) {
		// This is called when the last Cursor provided to onLoadFinished()
		// above is about to be closed. We need to make sure we are no
		// longer using it.
		mAdapter.swapCursor(null);
	}
	
	public void scrollToSelectedItem(int index) {
		int headerIndex = Integer.parseInt(dateDialogData.get(index));
		getListView().setSelection(headerIndex);
	}	
}