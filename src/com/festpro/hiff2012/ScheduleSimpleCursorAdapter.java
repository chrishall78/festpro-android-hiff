package com.festpro.hiff2012;

import java.util.HashSet;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ScheduleSimpleCursorAdapter extends SimpleCursorAdapter{

    Context context;
    Activity activity;

    public ScheduleSimpleCursorAdapter(Context context, int layout, Cursor c,
            String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, 0);
        this.context=context;
        this.activity=(Activity) context;
    }
    
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view = super.getView(position, convertView, parent);
        final int pos = position;
        ImageView image= (ImageView)view.findViewById(R.id.rowThumbSchedule);
        image.setOnClickListener(new View.OnClickListener() 
        {
            public void onClick(View v) 
            {
            	doMyFestToggle(v, pos);
            }
        });
        return view;
    }
    
    protected void doMyFestToggle(View v, int pos) {
    	ImageView imageView = (ImageView) v;
     	
        SharedPreferences preferences = 
                PreferenceManager.getDefaultSharedPreferences(activity);
    	Set<String> set = new HashSet<String>();
    	if ( preferences.contains("myFestArray") ) {        	
        	Cursor cursor = this.getCursor();
        	cursor.moveToPosition(pos);
            String film_id = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_FILM_ID));
        	String screening_id = cursor.getString(cursor.getColumnIndex(FestProDBAdapter.SCREENINGS_SCREENING_ID));

        	set = preferences.getStringSet("myFestArray", null);

            if (set != null) {
                if (set.contains(film_id+":"+screening_id)) {
                    set.remove(film_id+":"+screening_id);
                    imageView.setImageResource(R.drawable.icon_myfest_add_black);
                } else {
                    set.add(film_id+":"+screening_id);
                    imageView.setImageResource(R.drawable.icon_myfest_remove);
                }
            }

            // Remove MyFest Array from shared preferences, then add new array
            SharedPreferences.Editor localEditor = preferences.edit();
            localEditor.remove("myFestArray");
            localEditor.apply();

            localEditor.putStringSet("myFestArray", set);
            if (localEditor.commit()) {
                //Log.i("ScheduleSimpleCursorAdapter", "MyFest update failed.");
    		}        	
        }
    }
}
