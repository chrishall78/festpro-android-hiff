package com.festpro.hiff2012;

import java.util.ArrayList;
import java.util.Set;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class ScreeningDialog extends DialogFragment {
    AlertDialog alertDialog;

    static ScreeningDialog newInstance(String title, String filmTitle, ArrayList<String> itemText, ArrayList<String> itemData) {
		ScreeningDialog fragment = new ScreeningDialog();
		Bundle args = new Bundle();
		args.putString("title", title);
		args.putString("filmTitle", filmTitle);
		args.putStringArrayList("itemText", itemText);
		args.putStringArrayList("itemData", itemData);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {		
		String title = getArguments().getString("title");
		final String filmTitle = getArguments().getString("filmTitle");
		ArrayList<String> arrText = getArguments().getStringArrayList("itemText");
		ArrayList<String> arrData = getArguments().getStringArrayList("itemData");
		final String[] itemText = arrText.toArray(new String[arrText.size()]);
		final String[] itemData = arrData.toArray(new String[arrData.size()]);
				
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
		builder.setCancelable(true);
		builder.setTitle(title);
		
		if (title.equals(getActivity().getString(R.string.film_detail_popup_title01))) {
			builder.setIcon(R.drawable.icon_buytickets);
		} else if (title.equals(getActivity().getString(R.string.film_detail_popup_title03))) {
			builder.setIcon(R.drawable.icon_myfest_add);
		} else {
			builder.setIcon(R.drawable.ic_launcher);			
		}
		builder.setItems(itemText, new DialogInterface.OnClickListener() {
		    public void onClick(DialogInterface dialog, int item) {
		    	if (itemData[item].length() == 0) {
		    		// Blank ticket url?
		    		Toast.makeText(getActivity().getApplicationContext(), "Sorry, "+filmTitle+" ("+itemText[item]+") does not yet have a link to purchase tickets.", Toast.LENGTH_LONG).show();
				} else if (itemData[item].length() >= 15) {
			        if (itemData[item].substring(0, 5).equals("http:") || itemData[item].substring(0, 5).equals("https")) {
			        	/*
		    			// Open Web View with this URL
						Intent webIntent = new Intent(getActivity(), WebViewActivity.class);
						webIntent.setData(Uri.parse(itemData[item]));
				        startActivity(webIntent);
				        */

			        	// Open Web Browser with this URL
						Intent browserIntent = new Intent(Intent.ACTION_VIEW);
						browserIntent.setData(Uri.parse(itemData[item]));
				        startActivity(browserIntent);
			        }
		    	} else {
		    		// Length is less than 15, probably a MyFest ID - Add or remove from MyFest
		            SharedPreferences preferences = 
		                    PreferenceManager.getDefaultSharedPreferences(getActivity());
		        	Set<String> set;
		        	Boolean added = null;
		        	if ( preferences.contains("myFestArray") ) {        	
		            	set = preferences.getStringSet("myFestArray", null);

						if (set != null) {
							if (set.contains(itemData[item])) {
                                set.remove(itemData[item]);
                                added = false;
                            } else {
                                set.add(itemData[item]);
                                added = true;
                            }
						}

						SharedPreferences.Editor prefsEditor = preferences.edit();
		                prefsEditor.remove("myFestArray");
		                prefsEditor.apply();

		                prefsEditor.putStringSet("myFestArray", set);
		                if (prefsEditor.commit()) {
		                    //Log.i("ScreeningDialog", "MyFest update failed.");
		        		}
		            }		    		
		    		
		        	if (added != null) {
		        		if (added) {
					        Toast.makeText(getActivity().getApplicationContext(), filmTitle+" ("+itemText[item]+") was added to MyFest.", Toast.LENGTH_LONG).show();
		        		} else {
					        Toast.makeText(getActivity().getApplicationContext(), filmTitle+" ("+itemText[item]+") was removed from MyFest.", Toast.LENGTH_LONG).show();
		        		}		        		
		        	}
		    	}
		    }
		});
		alertDialog = builder.create();
		
        return alertDialog;
	}         

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);	
    }
	
	@Override
	public void onStart() {
		super.onStart();
	}
}
