package com.festpro.hiff2012;

import com.festpro.hiff2012.FilmsFragment;
import com.festpro.hiff2012.HomeFragment;
import com.festpro.hiff2012.MyFestFragment;
import com.festpro.hiff2012.ScheduleFragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.util.SparseArray;
import android.view.ViewGroup;

public class TabsPagerAdapter extends FragmentPagerAdapter {
	private SparseArray<String> mFragmentTags;	
	private FragmentManager mFragmentManager;
	
	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	    mFragmentManager = fm;
	    mFragmentTags = new SparseArray<String>();
	}

	@Override
	public int getCount() {
		return 4;
	}

	@Override
	public ListFragment getItem(int index) {
	    switch (index) {
		    case 0:
	            return new HomeFragment();
	        case 1:
	            return new FilmsFragment();
	        case 2:
	            return new ScheduleFragment();
	        case 3:
	            return new MyFestFragment();
        }
	    return null;
	}

	@Override
    public Object instantiateItem(ViewGroup container, int position) {
        Object obj = super.instantiateItem(container, position);
        if (obj instanceof ListFragment) {
            // record the fragment tag here.
            ListFragment f = (ListFragment) obj;
            String tag = f.getTag();
            mFragmentTags.put(position, tag);
        }
        return obj;
    }
	
	public ListFragment getFragment(int position) {
        String tag = mFragmentTags.get(position);
        if (tag == null) {
        	return null;
        }
        return (ListFragment) mFragmentManager.findFragmentByTag(tag);
    }
}