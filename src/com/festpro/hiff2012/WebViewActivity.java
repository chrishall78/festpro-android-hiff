package com.festpro.hiff2012;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebViewActivity extends Activity {
	WebView webView;
	Uri url;

	/** Called when the activity is first created. */
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar bar = getActionBar();
		if (bar != null) {
			bar.setDisplayHomeAsUpEnabled(true);
		}
		setContentView(R.layout.webview);

		url = getIntent().getData();
		webView = (WebView) findViewById(R.id.webView);
		webView.setWebChromeClient(new WebChromeClient());
		webView.setWebViewClient(new Callback());
		
		// Configure the webview
		WebSettings s = webView.getSettings();
		s.setBuiltInZoomControls(true);
		s.setDisplayZoomControls(false);
		s.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
		s.setUseWideViewPort(true);
		s.setLoadWithOverviewMode(true);
		s.setSaveFormData(true);
		s.setJavaScriptEnabled(true);
		s.setJavaScriptCanOpenWindowsAutomatically(true);
		// enable navigator.geolocation 
		// s.setGeolocationEnabled(false);
		// enable Web Storage: localStorage, sessionStorage
		s.setDomStorageEnabled(true);
		webView.loadUrl(url.toString());
	}

	@Override
	public void onStart() {
		super.onStart();

		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.setAcceptCookie(true);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		
		webView.stopLoading();
		webView.loadData("", "text/html", "utf-8");
		webView.reload();
		webView.setWebChromeClient(null);
		webView.setWebViewClient(null);
		webView.removeAllViews();
		webView.clearHistory();
		webView.destroy();
		webView = null;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.webview_activity, menu);
		return true;
	}	

	@Override
	public boolean onPrepareOptionsMenu (Menu menu) {
		String urlString = url.toString();
		if (!urlString.startsWith("https://") && !urlString.startsWith("http://")){
			menu.getItem(2).setEnabled(false);
		} else {
			menu.getItem(2).setEnabled(true);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				// app icon in action bar clicked; go back
				finish();
				return true;
			case R.id.wv_back_item:
				if (webView.canGoBack()) {
					webView.goBack();
					return true;
				} else {
					finish();
					return true;
				}
			case R.id.wv_forward_item:
				if (webView.canGoForward()) {
					webView.goForward();
					return true;
				} else {
					return false;
				}
			case R.id.wv_browser_item:
				Intent i = new Intent(Intent.ACTION_VIEW);
				i.setData(url);
				startActivity(i);
				return true;
			case R.id.wv_refresh_item:
				webView.reload();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	private class Callback extends WebViewClient {
		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			return(false);
		}

		@Override
		public void onPageFinished(WebView view, String url) {
			WebViewActivity.this.setTitle(view.getTitle());
		}

		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
			super.onReceivedSslError(view, handler, error);

			// this will ignore the SSL error and will go forward to your site
			handler.proceed();
		}
	}
}